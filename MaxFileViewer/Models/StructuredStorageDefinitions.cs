﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using Microsoft.VisualStudio.OLE.Interop;
using STATSTG = Microsoft.VisualStudio.OLE.Interop.STATSTG;
using IStream = Microsoft.VisualStudio.OLE.Interop.IStream;

namespace MaxFileViewer.Models
{
    static class StructuredStorageDefinitions
    {
        [Flags]
        public enum STGM : int
        {
            DIRECT = 0x00000000,
            TRANSACTED = 0x00010000,
            SIMPLE = 0x08000000,
            READ = 0x00000000,
            WRITE = 0x00000001,
            READWRITE = 0x00000002,
            SHARE_DENY_NONE = 0x00000040,
            SHARE_DENY_READ = 0x00000030,
            SHARE_DENY_WRITE = 0x00000020,
            SHARE_EXCLUSIVE = 0x00000010,
            PRIORITY = 0x00040000,
            DELETEONRELEASE = 0x04000000,
            NOSCRATCH = 0x00100000,
            CREATE = 0x00001000,
            CONVERT = 0x00020000,
            FAILIFTHERE = 0x00000000,
            NOSNAPSHOT = 0x00200000,
            DIRECT_SWMR = 0x00400000,
        }

        public enum ulKind : uint
        {
            PRSPEC_LPWSTR = 0,
            PRSPEC_PROPID = 1
        }

        public enum SumInfoProperty : uint
        {
            TITLE = 2,
            SUBJECT = 3,
            AUTHOR = 4,
            KEYWORDS = 5,
            COMMENTS = 6,
            TEMPLATE = 7,
            LASTAUTHOR = 8,
            REVNUMBER = 9,
            EDITTIME = 0xA,
            LASTPRINTED = 0xB,
            CREATE_DTM = 0xC,
            LASTSAVE_DTM = 0xD,
            PAGECOUNT = 0xE,
            WORDCOUNT = 0xF,
            CHARCOUNT = 0x10,
            THUMBNAIL = 0x11,
            APPNAME = 0x12,
            SECURITY = 0x13
        }

        public enum DocSumInfoProperty : uint
        {
            CATEGORY = 2,
            PRESFORMAT = 3,
            BYTECOUNT = 4,
            LINECOUNT = 5,
            PARCOUNT = 6,
            SLIDECOUNT = 7,
            NOTECOUNT = 8,
            HIDDENCOUNT = 9,
            MMCLIPCOUNT = 0xA,
            SCALE = 0xB,
            HEADINGPAIR = 0xC,
            DOCPARTS = 0xD,
            MANAGER = 0xE,
            COMPANY = 0xF,
            LINKSDIRTY = 0x10
        }

        [StructLayout(LayoutKind.Explicit)]
        public struct PROPVARIANTunion
        {
            [FieldOffset(0)]
            public sbyte cVal;
            [FieldOffset(0)]
            public byte bVal;
            [FieldOffset(0)]
            public short iVal;
            [FieldOffset(0)]
            public ushort uiVal;
            [FieldOffset(0)]
            public int lVal;
            [FieldOffset(0)]
            public uint ulVal;
            [FieldOffset(0)]
            public int intVal;
            [FieldOffset(0)]
            public uint uintVal;
            [FieldOffset(0)]
            public long hVal;
            [FieldOffset(0)]
            public ulong uhVal;
            [FieldOffset(0)]
            public float fltVal;
            [FieldOffset(0)]
            public double dblVal;
            [FieldOffset(0)]
            public short boolVal;
            [FieldOffset(0)]
            public int scode;
            [FieldOffset(0)]
            public long cyVal;
            [FieldOffset(0)]
            public double date;
            [FieldOffset(0)]
            public long filetime;
            [FieldOffset(0)]
            public IntPtr bstrVal;
            [FieldOffset(0)]
            public IntPtr pszVal;
            [FieldOffset(0)]
            public IntPtr pwszVal;
            [FieldOffset(0)]
            public IntPtr punkVal;
            [FieldOffset(0)]
            public IntPtr pdispVal;
            [FieldOffset(0)]
            public IntPtr puuid;
        }

        public struct PACKEDMETA
        {
            public ushort mm, xExt, yExt, reserved;
        }


        [DllImport("ole32.dll")]
        public static extern int StgOpenStorage(
            [MarshalAs(UnmanagedType.LPWStr)]string pwcsName, IStorage pstgPriority,
            int grfMode, IntPtr snbExclude, uint reserved, out IStorage ppstgOpen);

        [DllImport("ole32.dll")]
        public static extern int StgCreatePropSetStg(IStorage pStorage, uint reserved,
           out IPropertySetStorage ppPropSetStg);

        [DllImport("ole32.dll")]
        public static extern int StgCreatePropSetStg(IStorage pStorage, uint reserved,
           out Microsoft.VisualStudio.OLE.Interop.IPropertySetStorage ppPropSetStg);

        [DllImport("ole32.dll")]
        public extern static int PropVariantClear(ref PROPVARIANT pvar);


        [ComImport]
        [Guid("00000138-0000-0000-C000-000000000046")]
        [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        public interface IPropertyStorage
        {
            [PreserveSig]
            int ReadMultiple(uint cpspec,
                [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 0)] [In] PropertySpec[] rgpspec,
                [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 0)] [Out] PropertyVariant[] rgpropvar);

            [PreserveSig]
            void WriteMultiple(uint cpspec,
                [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 0)] [In] PropertySpec[] rgpspec,
                [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 0)] [In] PropertyVariant[] rgpropvar,
                uint propidNameFirst);

            [PreserveSig]
            uint DeleteMultiple(uint cpspec,
                [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 0)] [In] PropertySpec[] rgpspec);
            [PreserveSig]
            uint ReadPropertyNames(uint cpropid,
                [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 0)] [In] uint[] rgpropid,
                [MarshalAs(UnmanagedType.LPArray, ArraySubType = UnmanagedType.LPWStr, SizeParamIndex = 0)] [Out] string[] rglpwstrName);
            [PreserveSig]
            uint NotDeclared1();
            [PreserveSig]
            uint NotDeclared2();
            [PreserveSig]
            uint Commit(uint grfCommitFlags);
            [PreserveSig]
            uint NotDeclared3();
            [PreserveSig]
            uint Enum(out IEnumSTATPROPSTG ppenum);
        }

        [ComImport]
        [Guid("0000013A-0000-0000-C000-000000000046")]
        [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        public interface IPropertySetStorage
        {
            [PreserveSig]
            uint Create(ref Guid rfmtid, ref Guid pclsid, uint grfFlags, STGM grfMode, out IPropertyStorage ppprstg);
            [PreserveSig]
            uint Open(ref Guid rfmtid, STGM grfMode, out IPropertyStorage ppprstg);
            [PreserveSig]
            uint NotDeclared3();
            [PreserveSig]
            uint Enum(out IEnumSTATPROPSETSTG ppenum);
        }

        public enum PropertySpecKind
        {
            Lpwstr = 0,
            PropId = 1
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct PropertySpec
        {
            public PropertySpecKind kind;
            public PropertySpecData data;

            public string DisplayName(Type type)
            {
                
                var values = Enum.GetValues(type);
                foreach (var v in values)
                    if ((uint)v == data.propertyId)
                        return v.ToString();

                return string.Empty;
            }
        }

        [StructLayout(LayoutKind.Explicit)]
        public struct PropertySpecData
        {
            [FieldOffset(0)]
            public uint propertyId;
            [FieldOffset(0)]
            public IntPtr name;
        }
        /*
        [StructLayout(LayoutKind.Explicit)]
        public struct PropertyVariant
        {
            [FieldOffset(0)]
            public VarEnum vt;
            [FieldOffset(2)]
            public ushort wReserved1;
            [FieldOffset(4)]
            public ushort wReserved2;
            [FieldOffset(6)]
            public ushort wReserved3;
            [FieldOffset(8)]
            public PROPVARIANTunion unionmember;

            public string ValueToString()
            {
                switch (vt)
                {
                    case VarEnum.VT_EMPTY:
                    case VarEnum.VT_NULL:
                        return "";
                    case VarEnum.VT_I2:
                        return unionmember.iVal.ToString();
                    case VarEnum.VT_I4:
                    case VarEnum.VT_INT:
                        return unionmember.lVal.ToString();
                    case VarEnum.VT_I8:
                        return unionmember.hVal.ToString();
                    case VarEnum.VT_UI2:
                        return unionmember.uiVal.ToString();
                    case VarEnum.VT_UI4:
                    case VarEnum.VT_UINT:
                        return unionmember.ulVal.ToString();
                    case VarEnum.VT_UI8:
                        return unionmember.uhVal.ToString();
                    case VarEnum.VT_R4:
                        return unionmember.fltVal.ToString();
                    case VarEnum.VT_R8:
                        return unionmember.dblVal.ToString();
                    case VarEnum.VT_BSTR:
                        return unionmember.bstrVal.ToString();
                    case VarEnum.VT_ERROR:
                        return unionmember.scode.ToString();
                    case VarEnum.VT_BOOL:
                        return unionmember.boolVal == 0 ? "FALSE" : "TRUE";
                    case VarEnum.VT_I1:
                        return unionmember.cVal.ToString();
                    case VarEnum.VT_UI1:
                        return unionmember.bVal.ToString();
                    case VarEnum.VT_VOID:
                        return "";
                    case VarEnum.VT_LPSTR:
                        return Marshal.PtrToStringAnsi(unionmember.pszVal);
                    case VarEnum.VT_LPWSTR:
                        return Marshal.PtrToStringUni(unionmember.pwszVal);
                    case VarEnum.VT_FILETIME:
                        return DateTime.FromFileTime(unionmember.filetime).ToString();
                    case VarEnum.VT_CLSID:
                        return ((Guid)Marshal.PtrToStructure(unionmember.puuid, typeof(Guid))).ToString();
                    default:
                        return "Unknow value: " + (uint)vt;
                        return "...";
                }
            }
        }*/
        
        [StructLayout(LayoutKind.Explicit)]
        public struct PropertyVariant2
        {
            [FieldOffset(0)]
            public VarEnum vt;
            [FieldOffset(2)]
            short wReserved1;
            [FieldOffset(4)]
            short wReserved2;
            [FieldOffset(6)]
            short wReserved3;
            [FieldOffset(8)]
            sbyte cVal;
            [FieldOffset(8)]
            byte bVal;
            [FieldOffset(8)]
            short iVal;
            [FieldOffset(8)]
            ushort uiVal;
            [FieldOffset(8)]
            int lVal;
            [FieldOffset(8)]
            uint ulVal;
            [FieldOffset(8)]
            int intVal;
            [FieldOffset(8)]
            uint uintVal;
            [FieldOffset(8)]
            long hVal;
            [FieldOffset(8)]
            long uhVal;
            [FieldOffset(8)]
            float fltVal;
            [FieldOffset(8)]
            double dblVal;
            [FieldOffset(8)]
            bool boolVal;
            [FieldOffset(8)]
            int scode;
            //CY cyVal;
            [FieldOffset(8)]
            DateTime date;
            [FieldOffset(8)]
            System.Runtime.InteropServices.ComTypes.FILETIME filetime;
            [FieldOffset(8)]
            IntPtr puuid;
            //CLSID* puuid;
            //CLIPDATA* pclipdata;
            //BSTR bstrVal;
            //BSTRBLOB bstrblobVal;
            //[FieldOffset(8)]
            //Blob blobVal;
            //LPSTR pszVal;
            [FieldOffset(8)]
            IntPtr pwszVal; //LPWSTR 
            //IUnknown* punkVal;
            /*IDispatch* pdispVal;
            IStream* pStream;
            IStorage* pStorage;
            LPVERSIONEDSTREAM pVersionedStream;
            LPSAFEARRAY parray;
            CAC cac;
            CAUB caub;
            CAI cai;
            CAUI caui;
            CAL cal;
            CAUL caul;
            CAH cah;
            CAUH cauh;
            CAFLT caflt;
            CADBL cadbl;
            CABOOL cabool;
            CASCODE cascode;
            CACY cacy;
            CADATE cadate;
            CAFILETIME cafiletime;
            CACLSID cauuid;
            CACLIPDATA caclipdata;
            CABSTR cabstr;
            CABSTRBLOB cabstrblob;
            CALPSTR calpstr;
            CALPWSTR calpwstr;
            CAPROPVARIANT capropvar;
            CHAR* pcVal;
            UCHAR* pbVal;
            SHORT* piVal;
            USHORT* puiVal;
            LONG* plVal;
            ULONG* pulVal;
            INT* pintVal;
            UINT* puintVal;
            FLOAT* pfltVal;
            DOUBLE* pdblVal;
            VARIANT_BOOL* pboolVal;
            DECIMAL* pdecVal;
            SCODE* pscode;
            CY* pcyVal;
            DATE* pdate;
            BSTR* pbstrVal;
            IUnknown** ppunkVal;
            IDispatch** ppdispVal;
            LPSAFEARRAY* pparray;
            PROPVARIANT* pvarVal;
            */

            /// <summary>
            /// Helper method to gets blob data
            /// </summary>
            //byte[] GetBlob()
            //{
            //    byte[] Result = new byte[blobVal.Length];
            //    Marshal.Copy(blobVal.Data, Result, 0, Result.Length);
            //    return Result;
            //}

            /// <summary>
            /// Property value
            /// </summary>
            public object Value
            {
                get
                {
                    var v = (VarEnum)vt;
                    switch (v)
                    {
                        case VarEnum.VT_EMPTY:
                        case VarEnum.VT_NULL:
                            return "";
                        case VarEnum.VT_I2:
                            return iVal.ToString();
                        case VarEnum.VT_I4:
                        case VarEnum.VT_INT:
                            return lVal.ToString();
                        case VarEnum.VT_I8:
                            return hVal.ToString();
                        case VarEnum.VT_UI2:
                            return uiVal.ToString();
                        case VarEnum.VT_UI4:
                        case VarEnum.VT_UINT:
                            return ulVal.ToString();
                        case VarEnum.VT_UI8:
                            return uhVal.ToString();
                        case VarEnum.VT_R4:
                            return fltVal.ToString();
                        case VarEnum.VT_R8:
                            return dblVal.ToString();
                        //case VarEnum.VT_BSTR:
                        //    return bstrVal.ToString();
                        case VarEnum.VT_ERROR:
                            return scode.ToString();
                        //case VarEnum.VT_BOOL:
                        //    return boolVal == 0 ? "FALSE" : "TRUE";
                        case VarEnum.VT_I1:
                            return cVal.ToString();
                        case VarEnum.VT_UI1:
                            return bVal.ToString();
                        case VarEnum.VT_VOID:
                            return "";
                        //case VarEnum.VT_LPSTR:
                        //    return pszVal.ToString(); //String might be too big
                        //case VarEnum.VT_LPWSTR:
                        //    return unionmember.pwszVal.ToString(); //String might be too big
                        //case VarEnum.VT_FILETIME:
                        //    return new DateTime(date).ToString();
                        //case VarEnum.VT_CLSID:
                        //    return propVar. unionmember.puui.ToString(); 
                        default:
                            return "...";
                    }
                }
            }
        }
    }
}
