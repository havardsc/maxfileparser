﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Globalization;

namespace MaxFileViewer.Models
{
    [StructLayout(LayoutKind.Sequential)]
    public struct PropertyVariant
    {
        #region API
        [DllImport("Ole32.dll", PreserveSig = false)] // returns hresult
        internal extern static void PropVariantClear([In, Out] ref PropertyVariant pvar);

        [DllImport("Ole32.dll", PreserveSig = false)] // returns hresult
        internal extern static void PropVariantCopy([Out] out PropertyVariant pDst, [In] ref PropertyVariant pSrc);

        [DllImport("OleAut32.dll", PreserveSig = true)] // psa is actually returned, not hresult
        internal extern static IntPtr SafeArrayCreateVector(ushort vt, int lowerBound, uint cElems);

        [DllImport("OleAut32.dll", PreserveSig = false)] // returns hresult
        internal extern static IntPtr SafeArrayAccessData(IntPtr psa);

        [DllImport("OleAut32.dll", PreserveSig = false)] // returns hresult
        internal extern static void SafeArrayUnaccessData(IntPtr psa);

        [DllImport("OleAut32.dll", PreserveSig = true)] // retuns uint32
        internal extern static uint SafeArrayGetDim(IntPtr psa);

        [DllImport("OleAut32.dll", PreserveSig = false)] // returns hresult
        internal extern static int SafeArrayGetLBound(IntPtr psa, uint nDim);

        [DllImport("OleAut32.dll", PreserveSig = false)] // returns hresult
        internal extern static int SafeArrayGetUBound(IntPtr psa, uint nDim);

        [DllImport("OleAut32.dll", PreserveSig = false)] // returns hresult
        [return: MarshalAs(UnmanagedType.IUnknown)]
        internal extern static object SafeArrayGetElement(IntPtr psa, ref int rgIndices);

        [DllImport("OleAut32.dll", PreserveSig = false)]
        internal extern static void SafeArrayDestroy(IntPtr psa);

        [DllImport("propsys.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        internal static extern int InitPropVariantFromPropVariantVectorElem([In] ref PropertyVariant propvarIn, uint iElem, [Out] out PropertyVariant ppropvar);

        [DllImport("propsys.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        internal static extern uint InitPropVariantFromFileTime([In] ref System.Runtime.InteropServices.ComTypes.FILETIME pftIn, out PropertyVariant ppropvar);

        [DllImport("propsys.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.I4)]
        internal static extern int PropVariantGetElementCount([In] ref PropertyVariant propVar);

        [DllImport("propsys.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        internal static extern void PropVariantGetBooleanElem([In] ref PropertyVariant propVar, [In]uint iElem, [Out, MarshalAs(UnmanagedType.Bool)] out bool pfVal);

        [DllImport("propsys.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        internal static extern void PropVariantGetInt16Elem([In] ref PropertyVariant propVar, [In] uint iElem, [Out] out short pnVal);

        [DllImport("propsys.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        internal static extern void PropVariantGetUInt16Elem([In] ref PropertyVariant propVar, [In] uint iElem, [Out] out ushort pnVal);

        [DllImport("propsys.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        internal static extern void PropVariantGetInt32Elem([In] ref PropertyVariant propVar, [In] uint iElem, [Out] out int pnVal);

        [DllImport("propsys.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        internal static extern void PropVariantGetUInt32Elem([In] ref PropertyVariant propVar, [In] uint iElem, [Out] out uint pnVal);

        [DllImport("propsys.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        internal static extern void PropVariantGetInt64Elem([In] ref PropertyVariant propVar, [In] uint iElem, [Out] out Int64 pnVal);

        [DllImport("propsys.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        internal static extern void PropVariantGetUInt64Elem([In] ref PropertyVariant propVar, [In] uint iElem, [Out] out UInt64 pnVal);

        [DllImport("propsys.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        internal static extern void PropVariantGetDoubleElem([In] ref PropertyVariant propVar, [In] uint iElem, [Out] out double pnVal);

        [DllImport("propsys.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        internal static extern void PropVariantGetFileTimeElem([In] ref PropertyVariant propVar, [In] uint iElem, [Out, MarshalAs(UnmanagedType.Struct)] out System.Runtime.InteropServices.ComTypes.FILETIME pftVal);

        [DllImport("propsys.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        internal static extern void PropVariantGetStringElem([In] ref PropertyVariant propVar, [In]  uint iElem, [Out, MarshalAs(UnmanagedType.LPWStr)] out string ppszVal);

        [DllImport("propsys.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        internal static extern void InitPropVariantFromBooleanVector([In, Out] bool[] prgf, uint cElems, out PropertyVariant ppropvar);

        [DllImport("propsys.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        internal static extern void InitPropVariantFromInt16Vector([In, Out] Int16[] prgn, uint cElems, out PropertyVariant ppropvar);

        [DllImport("propsys.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        internal static extern void InitPropVariantFromUInt16Vector([In, Out] UInt16[] prgn, uint cElems, out PropertyVariant ppropvar);

        [DllImport("propsys.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        internal static extern void InitPropVariantFromInt32Vector([In, Out] Int32[] prgn, uint cElems, out PropertyVariant ppropvar);

        [DllImport("propsys.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        internal static extern void InitPropVariantFromUInt32Vector([In, Out] UInt32[] prgn, uint cElems, out PropertyVariant ppropvar);

        [DllImport("propsys.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        internal static extern void InitPropVariantFromInt64Vector([In, Out] Int64[] prgn, uint cElems, out PropertyVariant ppropvar);

        [DllImport("propsys.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        internal static extern void InitPropVariantFromUInt64Vector([In, Out] UInt64[] prgn, uint cElems, out PropertyVariant ppropvar);

        [DllImport("propsys.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        internal static extern void InitPropVariantFromDoubleVector([In, Out] double[] prgn, uint cElems, out PropertyVariant ppropvar);

        [DllImport("propsys.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        internal static extern void InitPropVariantFromFileTimeVector([In, Out] System.Runtime.InteropServices.ComTypes.FILETIME[] prgft, uint cElems, out PropertyVariant ppropvar);

        [DllImport("propsys.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        internal static extern void InitPropVariantFromStringVector([In, Out] string[] prgsz, uint cElems, out PropertyVariant ppropvar);
        #endregion
        #region Structures
        [StructLayout(LayoutKind.Explicit)]
        private struct PVDecimalOuterUnion
        {
            [FieldOffset(0)]
            public decimal decVal;

            [FieldOffset(0)]
            public PropertyVariant propVar;
        }

        [StructLayout(LayoutKind.Explicit)]
        private struct PVVectorOuterUnion
        {
            [FieldOffset(0)]
            public PropertyVariant propVar;

            [FieldOffset(8)]
            public uint cElems;

            [FieldOffset(12)]
            public IntPtr pElems;
        }
        #endregion
        #region Variables
        ushort valueType;
        ushort wReserved1;
        ushort wReserved2;
        ushort wReserved3;
        IntPtr valueData;
        int valueDataExt;
        #endregion
        #region Properties
        public VarEnum VarType
        {
            get { return (VarEnum)valueType; }
        }

        public object Value
        {
            get
            {
                switch ((VarEnum)valueType)
                {
                    case VarEnum.VT_I1:
                        return cVal;
                    case VarEnum.VT_UI1:
                        return bVal;
                    case VarEnum.VT_I2:
                        return iVal;
                    case VarEnum.VT_UI2:
                        return uiVal;
                    case VarEnum.VT_I4:
                    case VarEnum.VT_INT:
                        return lVal;
                    case VarEnum.VT_UI4:
                    case VarEnum.VT_UINT:
                        return ulVal;
                    case VarEnum.VT_I8:
                        return hVal;
                    case VarEnum.VT_UI8:
                        return uhVal;
                    case VarEnum.VT_R4:
                        return fltVal;
                    case VarEnum.VT_R8:
                        return dblVal;
                    case VarEnum.VT_BOOL:
                        return boolVal;
                    case VarEnum.VT_ERROR:
                        return scode;
                    case VarEnum.VT_CY:
                        return cyVal;
                    case VarEnum.VT_DATE:
                        return date;
                    case VarEnum.VT_FILETIME:
                        return DateTime.FromFileTime(hVal);
                    case VarEnum.VT_BSTR:
                        return Marshal.PtrToStringBSTR(valueData);
                    case VarEnum.VT_BLOB:
                        return GetBlobData();
                    case VarEnum.VT_LPSTR:
                        return Marshal.PtrToStringAnsi(valueData);
                    case VarEnum.VT_LPWSTR:
                        return Marshal.PtrToStringUni(valueData);
                    case VarEnum.VT_UNKNOWN:
                        return Marshal.GetObjectForIUnknown(valueData);
                    case VarEnum.VT_DISPATCH:
                        return Marshal.GetObjectForIUnknown(valueData);
                    case VarEnum.VT_DECIMAL:
                        return CrackDecimal();
                    case VarEnum.VT_CLSID:
                        return (Guid)Marshal.PtrToStructure(valueData, typeof(Guid));
                    case VarEnum.VT_ARRAY | VarEnum.VT_UNKNOWN:
                        return CrackSingleDimSafeArray(valueData);
                    case (VarEnum.VT_VECTOR | VarEnum.VT_LPWSTR):
                        return GetStringVector();
                    case (VarEnum.VT_VECTOR | VarEnum.VT_I2):
                        return GetVector<Int16>();
                    case (VarEnum.VT_VECTOR | VarEnum.VT_UI2):
                        return GetVector<UInt16>();
                    case (VarEnum.VT_VECTOR | VarEnum.VT_I4):
                        return GetVector<Int32>();
                    case (VarEnum.VT_VECTOR | VarEnum.VT_UI4):
                        return GetVector<UInt32>();
                    case (VarEnum.VT_VECTOR | VarEnum.VT_I8):
                        return GetVector<Int64>();
                    case (VarEnum.VT_VECTOR | VarEnum.VT_UI8):
                        return GetVector<UInt64>();
                    case (VarEnum.VT_VECTOR | VarEnum.VT_R8):
                        return GetVector<Double>();
                    case (VarEnum.VT_VECTOR | VarEnum.VT_BOOL):
                        return GetVector<Boolean>();
                    case (VarEnum.VT_VECTOR | VarEnum.VT_FILETIME):
                        return GetVector<DateTime>();
                    default:
                        return "...";
                        throw new NotSupportedException("The type of this variable is not support ('" + valueType.ToString(CultureInfo.CurrentCulture.NumberFormat) + "')");
                }
            }
        }

        sbyte cVal // CHAR cVal;
        {
            get { return (sbyte)GetDataBytes()[0]; }
        }

        byte bVal // UCHAR bVal;
        {
            get { return GetDataBytes()[0]; }
        }

        short iVal // SHORT iVal;
        {
            get { return BitConverter.ToInt16(GetDataBytes(), 0); }
        }

        ushort uiVal // USHORT uiVal;
        {
            get { return BitConverter.ToUInt16(GetDataBytes(), 0); }
        }

        int lVal // LONG lVal;
        {
            get { return BitConverter.ToInt32(GetDataBytes(), 0); }
        }

        uint ulVal // ULONG ulVal;
        {
            get { return BitConverter.ToUInt32(GetDataBytes(), 0); }
        }

        long hVal // LARGE_INTEGER hVal;
        {
            get { return BitConverter.ToInt64(GetDataBytes(), 0); }
        }

        ulong uhVal // ULARGE_INTEGER uhVal;
        {
            get { return BitConverter.ToUInt64(GetDataBytes(), 0); }
        }

        float fltVal // FLOAT fltVal;
        {
            get { return BitConverter.ToSingle(GetDataBytes(), 0); }
        }

        double dblVal // DOUBLE dblVal;
        {
            get { return BitConverter.ToDouble(GetDataBytes(), 0); }
        }

        bool boolVal // VARIANT_BOOL boolVal;
        {
            get { return (iVal == 0 ? false : true); }
        }

        int scode // SCODE scode;
        {
            get { return lVal; }
        }

        decimal cyVal // CY cyVal;
        {
            get { return decimal.FromOACurrency(hVal); }
        }

        DateTime date // DATE date;
        {
            get { return DateTime.FromOADate(dblVal); }
        }
        #endregion
        #region Public Methods
        public static PropertyVariant Empty
        {
            get
            {
                PropertyVariant empty = new PropertyVariant();
                empty.valueType = (ushort)VarEnum.VT_EMPTY;
                empty.wReserved1 = empty.wReserved2 = empty.wReserved3 = 0;
                empty.valueData = IntPtr.Zero;
                empty.valueDataExt = 0;
                return empty;
            }
        }

        public static PropertyVariant FromObject(object value)
        {
            PropertyVariant propVar = new PropertyVariant();

            if (value == null)
            {
                propVar.Clear();
                return propVar;
            }

            if (value.GetType() == typeof(string))
            {
                //Strings require special consideration, because they cannot be empty as well
                if (String.IsNullOrEmpty(value as string) || String.IsNullOrEmpty((value as string).Trim()))
                    throw new ArgumentException("String argument cannot be null or empty.");
                propVar.SetString(value as string);
            }
            else if (value.GetType() == typeof(bool?))
            {
                propVar.SetBool((value as bool?).Value);
            }
            else if (value.GetType() == typeof(bool))
            {
                propVar.SetBool((bool)value);
            }
            else if (value.GetType() == typeof(byte?))
            {
                propVar.SetByte((value as byte?).Value);
            }
            else if (value.GetType() == typeof(byte))
            {
                propVar.SetByte((byte)value);
            }
            else if (value.GetType() == typeof(sbyte?))
            {
                propVar.SetSByte((value as sbyte?).Value);
            }
            else if (value.GetType() == typeof(sbyte))
            {
                propVar.SetSByte((sbyte)value);
            }
            else if (value.GetType() == typeof(short?))
            {
                propVar.SetShort((value as short?).Value);
            }
            else if (value.GetType() == typeof(short))
            {
                propVar.SetShort((short)value);
            }
            else if (value.GetType() == typeof(ushort?))
            {
                propVar.SetUShort((value as ushort?).Value);
            }
            else if (value.GetType() == typeof(ushort))
            {
                propVar.SetUShort((ushort)value);
            }
            else if (value.GetType() == typeof(int?))
            {
                propVar.SetInt((value as int?).Value);
            }
            else if (value.GetType() == typeof(int))
            {
                propVar.SetInt((int)value);
            }
            else if (value.GetType() == typeof(uint?))
            {
                propVar.SetUInt((value as uint?).Value);
            }
            else if (value.GetType() == typeof(uint))
            {
                propVar.SetUInt((uint)value);
            }
            else if (value.GetType() == typeof(long?))
            {
                propVar.SetLong((value as long?).Value);
            }
            else if (value.GetType() == typeof(long))
            {
                propVar.SetLong((long)value);
            }
            else if (value.GetType() == typeof(ulong?))
            {
                propVar.SetULong((value as ulong?).Value);
            }
            else if (value.GetType() == typeof(ulong))
            {
                propVar.SetULong((ulong)value);
            }
            else if (value.GetType() == typeof(double?))
            {
                propVar.SetDouble((value as double?).Value);
            }
            else if (value.GetType() == typeof(double))
            {
                propVar.SetDouble((double)value);
            }
            else if (value.GetType() == typeof(decimal?))
            {
                propVar.SetDecimal((value as decimal?).Value);
            }
            else if (value.GetType() == typeof(decimal))
            {
                propVar.SetDecimal((decimal)value);
            }
            else if (value.GetType() == typeof(DateTime?))
            {
                propVar.SetDateTime((value as DateTime?).Value);
            }
            else if (value.GetType() == typeof(DateTime))
            {
                propVar.SetDateTime((DateTime)value);
            }
            else if (value.GetType() == typeof(string[]))
            {
                propVar.SetStringVector((value as string[]));
            }
            else if (value.GetType() == typeof(short[]))
            {
                propVar.SetShortVector((value as short[]));
            }
            else if (value.GetType() == typeof(ushort[]))
            {
                propVar.SetUShortVector((value as ushort[]));
            }
            else if (value.GetType() == typeof(int[]))
            {
                propVar.SetIntVector((value as int[]));
            }
            else if (value.GetType() == typeof(uint[]))
            {
                propVar.SetUIntVector((value as uint[]));
            }
            else if (value.GetType() == typeof(long[]))
            {
                propVar.SetLongVector((value as long[]));
            }
            else if (value.GetType() == typeof(ulong[]))
            {
                propVar.SetULongVector((value as ulong[]));
            }
            else if (value.GetType() == typeof(DateTime[]))
            {
                propVar.SetDateTimeVector((value as DateTime[]));
            }
            else if (value.GetType() == typeof(bool[]))
            {
                propVar.SetBoolVector((value as bool[]));
            }
            else
            {
                throw new ArgumentException("This Value type is not supported.");
            }

            return propVar;
        }

        public bool IsNull()
        {
            return (valueType == (ushort)VarEnum.VT_EMPTY || valueType == (ushort)VarEnum.VT_NULL);
        }

        public void Clear()
        {
            PropertyVariant var = this;
            PropVariantClear(ref var);

            valueType = (ushort)VarEnum.VT_EMPTY;
            wReserved1 = wReserved2 = wReserved3 = 0;
            valueData = IntPtr.Zero;
            valueDataExt = 0;
        }

        public PropertyVariant Clone()
        {
            PropertyVariant var = this;

            PropertyVariant clone;
            PropVariantCopy(out clone, ref var);

            return clone;
        }

        public void SetUInt(UInt32 value)
        {
            if (!IsNull()) Clear();

            valueType = (ushort)VarEnum.VT_UI4;
            valueData = (IntPtr)((int)value);
        }

        public void SetBool(bool value)
        {
            if (!IsNull()) Clear();

            valueType = (ushort)VarEnum.VT_BOOL;
            valueData = ((value == true) ? (IntPtr)65535 : (IntPtr)0);
        }

        public void SetDateTime(DateTime value)
        {
            if (!IsNull()) Clear();

            valueType = (ushort)VarEnum.VT_FILETIME;

            PropertyVariant propVar;
            System.Runtime.InteropServices.ComTypes.FILETIME ft = DateTimeTotFileTime(value);
            InitPropVariantFromFileTime(ref ft, out propVar);
            CopyData(propVar);
        }

        public void SetString(string value)
        {
            if (!IsNull()) Clear();

            valueType = (ushort)VarEnum.VT_LPWSTR;
            valueData = Marshal.StringToCoTaskMemUni(value);
        }

        public void SetIUnknown(object value)
        {
            if (!IsNull()) Clear();

            valueType = (ushort)VarEnum.VT_UNKNOWN;
            valueData = Marshal.GetIUnknownForObject(value);
        }

        public void SetSafeArray(Array array)
        {
            if (!IsNull()) Clear();
            if (array == null) return;

            const ushort vtUnknown = 13;
            IntPtr psa = SafeArrayCreateVector(vtUnknown, 0, (uint)array.Length);
            IntPtr pvData = SafeArrayAccessData(psa);

            try
            {
                for (int i = 0; i < array.Length; ++i)
                {
                    object obj = array.GetValue(i);
                    IntPtr punk = (obj != null) ? Marshal.GetIUnknownForObject(obj) : IntPtr.Zero;
                    Marshal.WriteIntPtr(pvData, i * IntPtr.Size, punk);
                }
            }
            finally
            {
                SafeArrayUnaccessData(psa);
            }

            valueType = (ushort)VarEnum.VT_ARRAY | (ushort)VarEnum.VT_UNKNOWN;
            valueData = psa;
        }

        public void SetByte(byte value)
        {
            if (!IsNull()) Clear();

            valueType = (ushort)VarEnum.VT_UI1;
            valueData = (IntPtr)value;
        }

        public void SetSByte(sbyte value)
        {
            if (!IsNull()) Clear();

            valueType = (ushort)VarEnum.VT_I1;
            valueData = (IntPtr)value;
        }

        public void SetShort(short value)
        {
            if (!IsNull()) Clear();

            valueType = (ushort)VarEnum.VT_I2;
            valueData = (IntPtr)value;
        }

        public void SetUShort(ushort value)
        {
            if (!IsNull()) Clear();

            valueType = (ushort)VarEnum.VT_UI2;
            valueData = (IntPtr)value;
        }

        public void SetInt(int value)
        {
            if (!IsNull()) Clear();

            valueType = (ushort)VarEnum.VT_I4;
            valueData = (IntPtr)value;
        }

        public void SetUIntVector(uint[] array)
        {
            if (!IsNull()) Clear();
            if (array == null) return;

            PropertyVariant propVar;
            InitPropVariantFromUInt32Vector(array, (uint)array.Length, out propVar);
            CopyData(propVar);
        }

        public void SetStringVector(string[] array)
        {
            if (!IsNull()) Clear();
            if (array == null) return;

            PropertyVariant propVar;
            InitPropVariantFromStringVector(array, (uint)array.Length, out propVar);
            CopyData(propVar);
        }

        public void SetBoolVector(bool[] array)
        {
            if (!IsNull()) Clear();
            if (array == null) return;

            PropertyVariant propVar;
            InitPropVariantFromBooleanVector(array, (uint)array.Length, out propVar);
            CopyData(propVar);
        }

        public void SetShortVector(short[] array)
        {
            if (!IsNull()) Clear();
            if (array == null) return;

            PropertyVariant propVar;
            InitPropVariantFromInt16Vector(array, (uint)array.Length, out propVar);
            CopyData(propVar);
        }

        public void SetUShortVector(ushort[] array)
        {
            if (!IsNull()) Clear();
            if (array == null) return;

            PropertyVariant propVar;
            InitPropVariantFromUInt16Vector(array, (uint)array.Length, out propVar);
            CopyData(propVar);
        }

        public void SetIntVector(int[] array)
        {
            if (!IsNull()) Clear();
            if (array == null) return;

            PropertyVariant propVar;
            InitPropVariantFromInt32Vector(array, (uint)array.Length, out propVar);
            CopyData(propVar);
        }

        public void SetLongVector(long[] array)
        {
            if (!IsNull()) Clear();
            if (array == null) return;

            PropertyVariant propVar;
            InitPropVariantFromInt64Vector(array, (uint)array.Length, out propVar);
            CopyData(propVar);
        }

        public void SetULongVector(ulong[] array)
        {
            if (!IsNull()) Clear();
            if (array == null) return;

            PropertyVariant propVar;
            InitPropVariantFromUInt64Vector(array, (uint)array.Length, out propVar);
            CopyData(propVar);
        }

        public void SetDoubleVector(double[] array)
        {
            if (!IsNull()) Clear();
            if (array == null) return;

            PropertyVariant propVar;
            InitPropVariantFromDoubleVector(array, (uint)array.Length, out propVar);
            CopyData(propVar);
        }

        public void SetDateTimeVector(DateTime[] array)
        {
            if (!IsNull()) Clear();
            if (array == null) return;

            System.Runtime.InteropServices.ComTypes.FILETIME[] fileTimeArr = new System.Runtime.InteropServices.ComTypes.FILETIME[array.Length];

            for (int i = 0; i < array.Length; i++)
            {
                fileTimeArr[i] = DateTimeTotFileTime(array[i]);
            }

            PropertyVariant propVar;
            InitPropVariantFromFileTimeVector(fileTimeArr, (uint)fileTimeArr.Length, out propVar);
            CopyData(propVar);
        }

        public void SetDecimal(decimal value)
        {
            if (!IsNull()) Clear();

            PVDecimalOuterUnion union = new PVDecimalOuterUnion();
            union.decVal = value;

            this = union.propVar;

            valueType = (ushort)VarEnum.VT_DECIMAL;
        }

        public void SetLong(long value)
        {
            if (!IsNull()) Clear();

            long[] valueArr = new long[] { value };
            PropertyVariant propVar;
            InitPropVariantFromInt64Vector(valueArr, 1, out propVar);
            CreatePropVariantFromVectorElement(propVar);
        }

        public void SetULong(ulong value)
        {
            if (!IsNull()) Clear();

            PropertyVariant propVar;
            ulong[] valueArr = new ulong[] { value };
            InitPropVariantFromUInt64Vector(valueArr, 1, out propVar);
            CreatePropVariantFromVectorElement(propVar);
        }

        public void SetDouble(double value)
        {
            if (!IsNull()) Clear();

            double[] valueArr = new double[] { value };
            PropertyVariant propVar;
            InitPropVariantFromDoubleVector(valueArr, 1, out propVar);
            CreatePropVariantFromVectorElement(propVar);
        }

        public static bool operator ==(PropertyVariant left, PropertyVariant right)
        {
            throw new NotImplementedException();
        }

        public static bool operator !=(PropertyVariant left, PropertyVariant right)
        {
            throw new NotImplementedException();
        }

        public override string ToString()
        {
            if (IsNull()) return "(null)";

            return "PropVariant: " + (VarEnum)valueType + ":" + Value;
        }

        public override bool Equals(object obj)
        {
            throw new NotImplementedException();
        }

        public override int GetHashCode()
        {
            throw new NotImplementedException();
        }
        #endregion
        #region Private Methods
        private void CopyData(PropertyVariant propVar)
        {
            valueType = propVar.valueType;
            valueData = propVar.valueData;
            valueDataExt = propVar.valueDataExt;
        }

        private void CreatePropVariantFromVectorElement(PropertyVariant propVar)
        {
            CopyData(propVar);
            InitPropVariantFromPropVariantVectorElem(ref this, 0, out propVar);
            CopyData(propVar);
        }

        private static long FileTimeToDateTime(ref System.Runtime.InteropServices.ComTypes.FILETIME val)
        {
            return (((long)val.dwHighDateTime) << 32) + val.dwLowDateTime;
        }

        private static System.Runtime.InteropServices.ComTypes.FILETIME DateTimeTotFileTime(DateTime value)
        {
            long hFT = value.ToFileTime();
            System.Runtime.InteropServices.ComTypes.FILETIME ft = new System.Runtime.InteropServices.ComTypes.FILETIME();
            ft.dwLowDateTime = (int)(hFT & 0xFFFFFFFF);
            ft.dwHighDateTime = (int)(hFT >> 32);
            return ft;
        }

        private object GetBlobData()
        {
            byte[] blobData = new byte[lVal];
            IntPtr pBlobData;

            if (IntPtr.Size == 4)
                pBlobData = new IntPtr(valueDataExt);
            else if (IntPtr.Size == 8)
                pBlobData = new IntPtr((Int64)(BitConverter.ToInt32(GetDataBytes(), sizeof(int))) + (Int64)(BitConverter.ToInt32(GetDataBytes(), 2 * sizeof(int)) << 32));
            else
                throw new NotSupportedException();

            Marshal.Copy(pBlobData, blobData, 0, lVal);
            return blobData;
        }

        private Array GetVector<T>() where T : struct
        {
            int count = PropVariantGetElementCount(ref this);
            if (count <= 0) return null;

            Array arr = new T[count];

            for (uint i = 0; i < count; i++)
            {
                if (typeof(T) == typeof(Int16))
                {
                    short val;
                    PropVariantGetInt16Elem(ref this, i, out val);
                    arr.SetValue(val, i);
                }
                else if (typeof(T) == typeof(UInt16))
                {
                    ushort val;
                    PropVariantGetUInt16Elem(ref this, i, out val);
                    arr.SetValue(val, i);
                }
                else if (typeof(T) == typeof(Int32))
                {
                    int val;
                    PropVariantGetInt32Elem(ref this, i, out val);
                    arr.SetValue(val, i);
                }
                else if (typeof(T) == typeof(UInt32))
                {
                    uint val;
                    PropVariantGetUInt32Elem(ref this, i, out val);
                    arr.SetValue(val, i);
                }
                else if (typeof(T) == typeof(Int64))
                {
                    long val;
                    PropVariantGetInt64Elem(ref this, i, out val);
                    arr.SetValue(val, i);
                }
                else if (typeof(T) == typeof(UInt64))
                {
                    ulong val;
                    PropVariantGetUInt64Elem(ref this, i, out val);
                    arr.SetValue(val, i);
                }
                else if (typeof(T) == typeof(DateTime))
                {
                    System.Runtime.InteropServices.ComTypes.FILETIME val;
                    PropVariantGetFileTimeElem(ref this, i, out val);

                    long fileTime = FileTimeToDateTime(ref val);
                    arr.SetValue(DateTime.FromFileTime(fileTime), i);
                }
                else if (typeof(T) == typeof(Boolean))
                {
                    bool val;
                    PropVariantGetBooleanElem(ref this, i, out val);
                    arr.SetValue(val, i);
                }
                else if (typeof(T) == typeof(Double))
                {
                    double val;
                    PropVariantGetDoubleElem(ref this, i, out val);
                    arr.SetValue(val, i);
                }
                else if (typeof(T) == typeof(String))
                {
                    string val;
                    PropVariantGetStringElem(ref this, i, out val);
                    arr.SetValue(val, i);
                }
            }

            return arr;
        }

        private string[] GetStringVector()
        {
            int count = PropVariantGetElementCount(ref this);
            if (count <= 0)
                return null;

            string[] strArr = new string[count];
            for (uint i = 0; i < count; i++)
            {
                PropVariantGetStringElem(ref this, i, out strArr[i]);
            }

            return strArr;
        }

        byte[] GetDataBytes()
        {
            byte[] ret = new byte[IntPtr.Size + sizeof(int)];

            if (IntPtr.Size == 4)
                BitConverter.GetBytes(valueData.ToInt32()).CopyTo(ret, 0);
            else if (IntPtr.Size == 8)
                BitConverter.GetBytes(valueData.ToInt64()).CopyTo(ret, 0);

            BitConverter.GetBytes(valueDataExt).CopyTo(ret, IntPtr.Size);
            return ret;
        }

        static Array CrackSingleDimSafeArray(IntPtr psa)
        {
            uint cDims = SafeArrayGetDim(psa);
            if (cDims != 1) return null;

            int lBound = SafeArrayGetLBound(psa, 1U);
            int uBound = SafeArrayGetUBound(psa, 1U);

            int n = uBound - lBound + 1;

            object[] array = new object[n];
            for (int i = lBound; i <= uBound; ++i)
            {
                array[i] = SafeArrayGetElement(psa, ref i);
            }

            return array;
        }

        decimal CrackDecimal()
        {
            PVDecimalOuterUnion union = new PVDecimalOuterUnion();
            union.propVar = this;
            decimal value = union.decVal;
            return value;
        }
        #endregion
    }
}
