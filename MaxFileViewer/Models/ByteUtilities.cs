﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MaxFileViewer.Models
{
    static class ByteUtilities
    {
        public static byte[] Concat(this byte[] arr1, byte[] arr2)
        {
            var arr3 = new byte[arr1.Length + arr2.Length];
            arr1.CopyTo(arr3, 0);
            arr2.CopyTo(arr3, arr1.Length);

            return arr3;
        }
    }
}
