﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using Microsoft.VisualStudio.OLE.Interop;
using STATSTG = Microsoft.VisualStudio.OLE.Interop.STATSTG;
using IStream = Microsoft.VisualStudio.OLE.Interop.IStream;

using SUtil = MaxFileViewer.Models.StructuredStorageDefinitions;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace MaxFileViewer.Models
{
    static class StructuredStorageUtilities
    {
        public static List<string> GetStreams(string filepath)
        {
            var streams = new List<string>();
            IStorage Is;
            var res = SUtil.StgOpenStorage(filepath, null, (int)(SUtil.STGM.SHARE_EXCLUSIVE | SUtil.STGM.READ), IntPtr.Zero, 0, out Is);
            var k = res.ToString("X4");
            if (res == 0 && Is != null)
            {
                IEnumSTATSTG SSenum;
                Is.EnumElements(0, IntPtr.Zero, 0, out SSenum);
                var SSstruct = new STATSTG[1];

                uint numReturned;

                do
                {
                    SSenum.Next(1, SSstruct, out numReturned);
                    if (numReturned != 0)
                        streams.Add(Regex.Replace(SSstruct[0].pwcsName, @"\d*", string.Empty));
                    
                } while (numReturned > 0);

                Marshal.ReleaseComObject(SSenum);
            }
            if (Is != null)
                Marshal.ReleaseComObject(Is);
            return streams;
        }

        public static IStream OpenStream(string filepath, string streamName, out IStorage Is, bool create)
        {
            var status = SUtil.StgOpenStorage(filepath, null, (int)(SUtil.STGM.SHARE_EXCLUSIVE | SUtil.STGM.READ), IntPtr.Zero, 0, out Is);
            var k = status.ToString("X4");
            if (status == 0 && Is != null)
            {
                IEnumSTATSTG SSenum;
                Is.EnumElements(0, IntPtr.Zero, 0, out SSenum);
                var SSstruct = new STATSTG[1];

                IStream dataStream;
                uint numReturned;
                bool exists = false;

                do
                {
                    SSenum.Next(1, SSstruct, out numReturned);
                    if (numReturned != 0)
                    {
                        //Console.WriteLine(SSstruct[0].pwcsName + " " + SSstruct[0].type + " " + SSstruct[0].cbSize.QuadPart);
                        if ((!SSstruct[0].pwcsName.Contains(streamName)))
                            continue;
                        exists = true;
                        break;
                    }
                } while (numReturned > 0);

                if (!exists && create)
                {
                    Is.CreateStream(
                        streamName,
                        (uint)(SUtil.STGM.CREATE | SUtil.STGM.WRITE | SUtil.STGM.DIRECT | SUtil.STGM.SHARE_EXCLUSIVE),
                        0, 0,
                        out dataStream);
                }
                else if (exists)
                {
                    Is.OpenStream(
                        SSstruct[0].pwcsName,
                        IntPtr.Zero,
                        (uint)(SUtil.STGM.SHARE_EXCLUSIVE | SUtil.STGM.READ),
                        0, out dataStream);
                }
                else
                {
                    return null;
                }

                Marshal.ReleaseComObject(SSenum);
                //Marshal.FinalReleaseComObject(SSenum);

                return dataStream;
            }
            else
                throw new ArgumentNullException("Stream is null");
        }
    }
}
