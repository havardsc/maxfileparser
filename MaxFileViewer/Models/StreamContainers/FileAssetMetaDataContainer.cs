﻿using System;
using Microsoft.VisualStudio.OLE.Interop;
using System.Runtime.InteropServices;
using System.Collections.Generic;

using STATSTG = Microsoft.VisualStudio.OLE.Interop.STATSTG;
using IStream = Microsoft.VisualStudio.OLE.Interop.IStream;
using System.Text;
using System.ComponentModel;

namespace MaxFileViewer.Models
{
    class FileAssetMetaDataContainer : StreamContainer
    {
        public override string StreamName { get { return "FileAssetMetaData"; } }

        public List<FileAssetMetaData> Assets { get; private set; }

        public FileAssetMetaDataContainer(string path) :
            base(path) { }

        public override string ToString()
        {
            if (Assets == null)
                return "Assets not retrieved";

            var s = "Assets:";
            foreach (var asset in Assets)
                s += "\n\t" + asset.ToString();
            
            return s;
        }



        public override bool ReadStream(BackgroundWorker bgworker = null, MaxStreamsContainer maxContainer = null)
        {
            if (!base.ReadStream())
                return false;

            Assets = new List<FileAssetMetaData>();

            try
            {
                var stat = new STATSTG[1];
                stream.Stat(stat, 0);
                uint total = 0;

                int guidSize = 16;
                int typeSizeSize = 4;
                StreamSize = stat[0].cbSize.QuadPart;
                while (StreamSize > total)
                {
                    var guid = ReadGuid(stream, guidSize, ref total);
                    var typeSize = ReadSize(stream, typeSizeSize, ref total);
                    var type = ReadString(stream, typeSize, ref total);
                    var pathSize = ReadSize(stream, typeSizeSize, ref total);
                    var path = ReadString(stream, pathSize, ref total);

                    var asset = new FileAssetMetaData(guid, typeSize, pathSize, type, path);
                    Assets.Add(asset);

                    if (bgworker != null)
                        bgworker.ReportProgress((int)(100 * total / StreamSize));
                }

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }
    }
}
