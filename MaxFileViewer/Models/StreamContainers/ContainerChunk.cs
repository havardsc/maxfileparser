﻿using System;
using Microsoft.VisualStudio.OLE.Interop;
using System.Runtime.InteropServices;
using System.Collections.Generic;

using STATSTG = Microsoft.VisualStudio.OLE.Interop.STATSTG;
using IStream = Microsoft.VisualStudio.OLE.Interop.IStream;
using System.Text;
using System.Linq;
using System.Collections.Specialized;

namespace MaxFileViewer.Models
{
    class ContainerChunk : DataChunk
    {
        public StreamContainer Stream { get; protected set; }

        public List<DataChunk> Children { get; protected set; }

        public ContainerChunk(StreamContainer stream, byte[] header, ulong size, ulong headerSize, int depth, bool parseChildren)
            : base(header, size, headerSize, null, depth)
        {
            if (parseChildren)
                ReadChildren(stream, size, headerSize);
        }

        public ContainerChunk(StreamContainer stream, byte[] header, ulong size, ulong headerSize, int depth)
            : base(header, size, headerSize, null, depth)
        {
            ReadChildren(stream, size, headerSize);
        }

        protected virtual void ReadChildren(StreamContainer stream, ulong size, ulong headerSize)
        {
            Stream = stream; 
            var chunkSize = stream.TotalRead + size - headerSize;
            while (stream.StreamSize > stream.TotalRead && chunkSize > stream.TotalRead)
            {
                var child = stream.ParseChunk<DataChunk, ContainerChunk>(stream, Depth + 1);
                AddChild(child);
            }
        }

        public void AddChild(DataChunk child)
        {
            if (Children == null)
                Children = new List<DataChunk>();
            Children.Add(child);
        }

        public override string ToString()
        {
            return string.Format("Header:{0}: Size:{1} Children:{2}",
                StreamContainer.ByteToHexBitFiddle(Header.Reverse().ToArray()),
                Size,
                Children != null ? Children.Count : 0);
        }

        public override string FormattedPrint(int index)
        {
            string tabs = "";
            for (int i = 0; i < Depth; i++) tabs += "\t";
            tabs += index + " " + ToString() + "\n";
            
            for (int i = 0; i < Children.Count; i++)
                tabs += Children[i].FormattedPrint(i);

            return tabs;// +"\n";
        }
    }
}
