﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MaxFileViewer.Models
{
    class LayerEntry
    {
        public SceneEntry SceneEntry { get; private set; }
        public List<SceneEntry> Children { get; set; }
        public int ID { get; private set; }

        public LayerEntry(SceneEntry sceneEntry, int id)
        {
            SceneEntry = sceneEntry;
            ID = id;
            Children = new List<SceneEntry>();
        }

        public override string ToString()
        {
            return SceneEntry.ToString();
        }
    }
}
