﻿using System;
using Microsoft.VisualStudio.OLE.Interop;
using System.Runtime.InteropServices;
using System.Collections.Generic;

using STATSTG = Microsoft.VisualStudio.OLE.Interop.STATSTG;
using IStream = Microsoft.VisualStudio.OLE.Interop.IStream;
using System.Text;
using System.Linq;

namespace MaxFileViewer.Models
{
    class SceneEntry : ContainerChunk
    {
        static byte[] refContainerHeader = new byte[] {22, 0};
        static byte[] refChunkHeader0 = new byte[] {53, 32};
        static byte[] refChunkHeader1 = new byte[] { 52, 32 };
        static byte[] refParentChunkHeder = new byte[] { 96, 9 };
        static byte[] refNameChunkHeader = new byte[] { 98, 9 };

        static int SceneEntryCounter = 0;

        const int ControllerIDIndex = 0;
        const int ObjectIDIndex = 1;
        const int MaterialIDIndex = 3;
        const int LayerIDIndex = 6;

        SceneContainer sceneContainerStream;
        int[] refIDs;
        int parentRefId = -1;
        ClassDirectoryEntry classdirEntry;
        DllDirectoryEntry dllInfo;
        DataChunk[] refs;
        DataChunk parent;
        string nodeName;

        public bool IsRefArray { get; private set; }

        public int ID { get; private set; }

        SceneContainer SceneContainerStream
        {
            get
            {
                sceneContainerStream = sceneContainerStream == null ? (SceneContainer)Stream : sceneContainerStream;
                return sceneContainerStream;
            }
        }

        public int ClassIndex { get; private set; }
        
        public ClassDirectoryEntry Class
        {
            get
            {
                if (classdirEntry == null)
                {
                    if (ClassIndex == int.MinValue || SceneContainerStream.ClassDirectoryContainer == null)
                        return null;
                    classdirEntry =
                        (SceneContainerStream.ClassDirectoryContainer.
                        StreamChunks[ClassIndex] as ClassDirectoryEntry);
                }
                return classdirEntry;
            }
        }

        public DllDirectoryEntry DllInfo
        {
            get
            {
                if (dllInfo == null)
                {
                    if (ClassIndex == int.MinValue || 
                        SceneContainerStream.DllDirectoryContainer == null || 
                        Class.DllIndex < 0)
                        return null;

                    dllInfo = (SceneContainerStream.DllDirectoryContainer.DllEntries[Class.DllIndex]);
                }
                return dllInfo;
            }
        }
        
        public int[] ReferenceIDs
        {
            get
            {
                if (refIDs == null)
                {
                    var refChunk = Children.FirstOrDefault(i => CompareHeaders(i.Header, refChunkHeader0) || CompareHeaders(i.Header, refChunkHeader1));

                    if (refChunk == null)
                        return null;

                    IsRefArray = CompareHeaders(refChunk.Header, refChunkHeader1);

                    refIDs = new int[refChunk.Data.Length / 4];
                    for (int i = 0; i < refIDs.Length; i++)
                        refIDs[i] = BitConverter.ToInt32(refChunk.Data, i * 4);

                }
                return refIDs;
            }
        }
        
        public DataChunk[] References
        {
            get
            {
                if (refs == null)
                {
                    if (ReferenceIDs == null)
                        return null;

                    //refs = new DataChunk[ReferenceIDs.Length];
                    var refsList = new List<DataChunk>();
                    var entries = (sceneContainerStream.StreamChunks[0] as ContainerChunk).Children;

                    for (int i = 0; i < ReferenceIDs.Length; i++)
                        if (ReferenceIDs[i] > -1)//refs[i] = entries[ReferenceIDs[i]];
                            refsList.Add(entries[ReferenceIDs[i]]);
                    refs = refsList.ToArray();
                }
                return refs;
            }
        }

        public int ParentReferenceID
        {
            get
            {
                if (parentRefId == -1)
                {
                    var parentChunk = Children.FirstOrDefault(i => CompareHeaders(i.Header, refParentChunkHeder));
                    if (parentChunk == null)
                        return -1;

                    parentRefId = BitConverter.ToInt32(parentChunk.Data.Take(4).ToArray(), 0);
                }
                return parentRefId;
            }
        }

        public DataChunk Parent
        {
            get
            {
                if (parent == null)
                {
                    if (ParentReferenceID == -1)
                        return null;
                    parent = (sceneContainerStream.StreamChunks[0] as ContainerChunk).Children[ParentReferenceID];
                }
                return parent;
            }
        }

        public string NodeName
        {
            get
            {
                if (nodeName == null)
                {
                    var nameChunk = Children.FirstOrDefault(i => CompareHeaders(i.Header, refNameChunkHeader));
                    if (nameChunk == null)
                        return null;

                    nodeName = nameChunk.DataString;
                }
                return nodeName;
            }
            set
            {
                nodeName = value;
            }
        }

        public SceneEntry MaterialChunk { get { return GetRefChunkByID(MaterialIDIndex); } }
        public SceneEntry ControllerChunk { get { return GetRefChunkByID(ControllerIDIndex); } }
        public SceneEntry LayerChunk { get { return GetRefChunkByID(LayerIDIndex); } }
        public SceneEntry ObjectChunk { get { return GetRefChunkByID(ObjectIDIndex); } }

        public List<SceneEntry> SceneEntryChildren { get; private set; }

        public SceneEntry(ContainerChunk chunk, int classIndex)
            : base(chunk.Stream, chunk.Header, chunk.Size, chunk.HeaderSize, chunk.Depth, false) 
        {
            ID = SceneEntryCounter++;
            ClassIndex = classIndex;
            sceneContainerStream = chunk.Stream as SceneContainer;
            Children = chunk.Children;

            //AddToLayer();
        }

        public SceneEntry(SceneContainer stream, byte[] header, ulong size, ulong headerSize, int depth)
            : base(stream, header, size, headerSize, depth) 
        {
            ID = SceneEntryCounter++;
            ClassIndex = int.MinValue;
        }

        public void AddChild(SceneEntry child)
        {
            if (SceneEntryChildren == null)
                SceneEntryChildren = new List<SceneEntry>();

            SceneEntryChildren.Add(child);
        }

        public string NodeToString(int depth, int childNum)
        {
            var tabs = "";
            for (int i = 0; i < depth; i++)
                tabs += "\t";
            
            var s = string.Format(
                "\n{0}{1}: {2}{3}{4}{5}{6}\n", 
                tabs, 
                childNum, 
                NodeName, 
                ObjectChunk != null ? "\n\t" + tabs +"Type: " + ObjectChunk.Class.ToString() : string.Empty,
                LayerChunk != null ? "\n\t" + tabs + "Layer: " + LayerChunk.Class.ToString() : string.Empty,
                MaterialChunk != null ? "\n\t" + tabs + "Material: " + MaterialChunk.Class.ToString() : string.Empty,
                ControllerChunk != null ? "\n\t" + tabs + "Controller: " + ControllerChunk.Class.ToString() : string.Empty
            );
            
            if (SceneEntryChildren != null)
                for (int i = 0; i < SceneEntryChildren.Count; i++ )
                    s += SceneEntryChildren[i].NodeToString(depth + 1, i);
            return s;
        }

        public override string ToString()
        {
            //if (NodeName == null)
                return string.Format("{0}: {1} {2} {3}",
                    ID,
                    Class != null ? Class.Description : string.Empty,
                    DllInfo != null ? Class != null ? Class.DllIndex < 0 ? "Built in" : DllInfo.Name : "Built in" : string.Empty,
                    base.ToString());

            //return string.Format("{0} {1}",
            //    ID, 
            //    NodeName, 

        }

        public static bool CompareHeaders(byte[] h1, byte[] h2)
        {
            return h1[0] == h2[0] && h1[1] == h2[1];
        }

        public void AddToLayer(List<LayerEntry> layerContainer)
        {
            var layerChunk = LayerChunk;
            if (layerChunk != null)
            {
                LayerEntry layer = layerContainer.FirstOrDefault(i => i.ID == layerChunk.ID);
                if (layer == null)
                {
                    layer = new LayerEntry(layerChunk, layerChunk.ID);
                    layerContainer.Add(layer);
                }

                layer.Children.Add(this);
            }
            if (SceneEntryChildren != null)
                foreach (var child in SceneEntryChildren)
                    child.AddToLayer(layerContainer);
        }

        SceneEntry GetRefChunkByID(int id)
        {
            if (ReferenceIDs == null || IsRefArray)
                return null;

            int objIndex = -1;
            for (int i = 0; i < ReferenceIDs.Length; i++)
                if (ReferenceIDs[i] == id)
                {
                    objIndex = i + 1;
                    break;
                }
            if (objIndex == -1)
                return null;

            return (References[objIndex] as SceneEntry);
        }
    }
}