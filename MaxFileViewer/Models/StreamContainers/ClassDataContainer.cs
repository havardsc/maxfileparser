﻿using System;
using Microsoft.VisualStudio.OLE.Interop;
using System.Runtime.InteropServices;
using System.Collections.Generic;

using STATSTG = Microsoft.VisualStudio.OLE.Interop.STATSTG;
using IStream = Microsoft.VisualStudio.OLE.Interop.IStream;
using System.Text;
using System.Linq;
using System.ComponentModel;

namespace MaxFileViewer.Models
{
    class ClassDataContainer : StreamContainer
    {
        public override string StreamName { get { return "ClassData"; } }

        public ClassDataContainer(string filepath)
            : base(filepath) { }

        public override bool ReadStream(BackgroundWorker bgworker = null, MaxStreamsContainer maxContainer = null)
        {
            if (!base.ReadStream())
                return false;
            
            try
            {
                var stat = new STATSTG[1];
                stream.Stat(stat, 0);
                StreamSize = stat[0].cbSize.QuadPart;

                while (StreamSize > TotalRead)
                {
                    var chunk = ParseChunk<DataChunk, ClassDataEntry>(this, 0);
                    StreamChunks.Add(chunk);
                    
                    if (bgworker != null)
                        bgworker.ReportProgress((int)(100 * TotalRead / StreamSize));
                }
            
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }
    }
}