﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IStream = Microsoft.VisualStudio.OLE.Interop.IStream;

namespace MaxFileViewer.Models
{
    class DataChunk
    {
        public byte[] Header { get; private set; }
        public ulong Size { get; private set; }
        public ulong HeaderSize { get; private set; }
        public virtual byte[] Data { get; protected set; }
        public int Depth { get; private set; }

        public string DataString 
        { 
            get 
            { 
                return Data != null ? Encoding.Unicode.GetString(Data) : string.Empty; 
            } 
        }

        public string DataHexString
        {
            get
            {
                return Data != null ?  StreamContainer.ByteToHexBitFiddle(Data) : string.Empty;
            }
        }

        public string IntegerString
        {
            get
            {
                if (Data == null)
                    return string.Empty;
                var strings = new string[Data.Length / 4];
                for (int i = 0; i < strings.Length; i++)
                    strings[i] = BitConverter.ToInt32(Data, i * 4).ToString();

                return string.Join(", ", strings);
            }
        }

        public DataChunk() { }

        public DataChunk(
            byte[] header, 
            ulong size, 
            ulong headerSize,
            byte[] data,
            int depth)
        {
            Header = header;
            Size = size;
            HeaderSize = headerSize;
            Data = data;
            Depth = depth;
        }

        public override string ToString()
        {
            return string.Format("Header:{0} String:{1} Int:{2} Hex:{3}",
                StreamContainer.ByteToHexBitFiddle(Header.Reverse().ToArray()),
                DataString,
                IntegerString,
                DataHexString);
        }

        public virtual string FormattedPrint(int index)
        {
            string tabs = "";
            for (int i = 0; i < Depth; i++) tabs += "\t";
            return tabs + index + " " + ToString() + "\n";
        }
    }
}
