﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MaxFileViewer.Models
{
    class FileAssetMetaData
    {
        string data;
        string typeName;

        public string Data
        {
            get { return data.Remove(data.Length - 1); }
            set
            {
                if (value == data) return;
                data = value;
                DataSize = value.Length;
                Dirty = true;
            }
        }

        public string TypeName 
        {
            get { return typeName.Remove(typeName.Length - 1); }
            private set
            {
                if (value == typeName) return;
                typeName = value;
            }
        }

        public bool Dirty { get; private set; }
        public Guid Guid { get; private set; }
        public int TypeSize { get;  private set; }
        public int DataSize { get;  private set; }
        
        public FileAssetMetaData(Guid guid, int typeSize, int dataSize, string typeName, string data)
        {
            Guid = guid;
            TypeSize = typeSize;
            TypeName = typeName;
            DataSize = dataSize;
            this.data = data;
        }

        public override string ToString()
        {
            return string.Format("{0}: {1} \t{2}", TypeName, Data, Guid);
        }
    }
}
