﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IStream = Microsoft.VisualStudio.OLE.Interop.IStream;

namespace MaxFileViewer.Models
{
    class ClassDirectoryEntry : ContainerChunk
    {
        DllDirectoryEntry dllInfo;

        public int DllIndex
        {
            get 
            { 
                return Children.Count > 0 ?  
                    BitConverter.ToInt32(Children[0].Data.Take(4).ToArray<byte>(), 0) : int.MinValue; 
            }
        }

        public DllDirectoryEntry DllInfo
        {
            get
            {
                if (dllInfo == null)
                {
                    if (DllIndex >= 0 && ClassDirectoryContainer.DllDirectoryContainer != null)
                        dllInfo = ClassDirectoryContainer.DllDirectoryContainer.
                            DllEntries[DllIndex];
                }

                return dllInfo;
            }
        }

        public string DllName
        {
            get
            {
                if (DllIndex == -1)
                    return "Built in";
                else if (DllInfo != null)
                    return DllInfo.Name;
                return string.Empty;
            }
        }

        public uint[] ClassID
        {
            get 
            {
                return Children.Count > 0 ? 
                    new uint[]
                    {
                        BitConverter.ToUInt32(Children[0].Data.Skip(4).Take(4).ToArray(), 0),
                        BitConverter.ToUInt32(Children[0].Data.Skip(8).Take(4).ToArray(), 0)
                    } :
                    null;
            }
        }

        public uint SuperClassID
        {
            get 
            { 
                return Children.Count > 0 ? 
                    BitConverter.ToUInt32(Children[0].Data.Skip(12).Take(4).ToArray(), 0) :
                    uint.MaxValue; 
            }
        }

        public string Description
        {
            get 
            { 
                return Children.Count > 1 ? 
                    Encoding.Unicode.GetString(Children[1].Data.ToArray<byte>()) :
                    string.Empty; 
            }
        }

        public ClassDirectoryContainer ClassDirectoryContainer { get; private set; }

        public ClassDirectoryEntry(StreamContainer stream, byte[] header, ulong size, ulong headerSize, int depth)
            : base(stream, header, size, headerSize, depth)
        {
            ClassDirectoryContainer = stream as ClassDirectoryContainer;
        }
            

        //public override string ToString()
        //{
        //    return
        //        String.Format(
        //        "{0} DllIndex:{1} ClassID:#({2}) SuperclassID:{3}",
        //        Description,
        //        DllIndex > 0 ? DllIndex.ToString() : " Builtin",
        //        ClassID[0].ToString("X4") + ", " + ClassID[1].ToString("X4"),
        //        SuperClassID);
        //}
    }
}
