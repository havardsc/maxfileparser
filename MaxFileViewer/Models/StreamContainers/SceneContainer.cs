﻿using System;
using Microsoft.VisualStudio.OLE.Interop;
using System.Runtime.InteropServices;
using System.Collections.Generic;

using STATSTG = Microsoft.VisualStudio.OLE.Interop.STATSTG;
using IStream = Microsoft.VisualStudio.OLE.Interop.IStream;
using System.Text;
using System.Linq;
using System.ComponentModel;

namespace MaxFileViewer.Models
{
    class SceneContainer : StreamContainer
    {
        SceneEntry rootNode;
        BackgroundWorker bgworker;
        int progress;
        List<LayerEntry> layers;

        public override string StreamName { get { return "Scene"; } }

        public DllDirectoryContainer DllDirectoryContainer
        {
            get { return ClassDirectoryContainer.DllDirectoryContainer; }
        }
        public ClassDirectoryContainer ClassDirectoryContainer { get; private set; }

        public List<LayerEntry> Layers
        {
            get
            {
                if (layers == null)
                {
                    layers = new List<LayerEntry>();
                    RootNode.AddToLayer(layers);
                    //foreach (var node in RootNode.SceneEntryChildren)
                    //    node.AddToLayer(layers);
                }
                return layers;
            }
        }

        public SceneEntry RootNode
        {
            get
            {
                if (rootNode == null)
                {
                    var entries = (StreamChunks[0] as ContainerChunk).Children.
                    Where(i => i is SceneEntry).Select(i => i as SceneEntry);

                    foreach (var node in entries)
                        if (node.Parent != null)
                            (node.Parent as SceneEntry).AddChild(node);

                    rootNode =
                        entries.
                        FirstOrDefault(k => (k as SceneEntry).
                        Class.Description == "RootNode") as SceneEntry;
                    rootNode.NodeName = "Root node";
                }
                return rootNode;
            }
        }

        public SceneContainer(string filepath) 
            : base(filepath) 
        {
            progress = 0;
        }

        public override bool ReadStream(BackgroundWorker bgworker = null, MaxStreamsContainer maxContainer = null)
        {
            if (maxContainer != null)
            {
                ClassDirectoryContainer =
                    maxContainer.GetStream("ClassDirectory", true) as ClassDirectoryContainer;
                ClassDirectoryContainer.ReadStream(bgworker, maxContainer);
                ClassDirectoryContainer.Dispose();
            }

            if (!base.ReadStream())
                return false;

            this.bgworker = bgworker;
            var sw = new System.Diagnostics.Stopwatch();
            sw.Start();
            try
            {
                var stat = new STATSTG[1];
                stream.Stat(stat, 0);
                StreamSize = stat[0].cbSize.QuadPart;

                while (StreamSize > TotalRead)
                {
                    var chunk = ParseChunk<DataChunk, SceneEntry>(this, 0);
                    StreamChunks.Add(chunk);
                }
                sw.Stop();
                Console.WriteLine("TIME: " + sw.ElapsedMilliseconds);
                var l = Layers;
                Console.WriteLine(l.Count);
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }
        
        public override DataChunk ParseChunk<T, K>(StreamContainer stream, int depth)
        {
            DataChunk chunk;
            chunk = base.ParseChunk<T, K>(stream, depth);
            //return chunk;
            
            if (bgworker != null && chunk.Depth == 1)
            {
                var newProg = (int)(100 * TotalRead / StreamSize);
                if (newProg > progress)
                {
                    progress = newProg;
                    bgworker.ReportProgress(newProg);
                }
            }

            if (chunk.Depth != 1 || !(chunk is ContainerChunk))
                return chunk;

            var classIndex = chunk.Header[0] | (chunk.Header[1] << 8);
            var container = chunk as ContainerChunk;

            if (classIndex < 0 || 
                ClassDirectoryContainer == null || 
                classIndex >= ClassDirectoryContainer.StreamChunks.Count)
                return chunk;
            
            chunk = new SceneEntry(chunk as ContainerChunk, classIndex);
            return chunk;
        }

    }
}
