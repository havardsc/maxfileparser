﻿using System;
using Microsoft.VisualStudio.OLE.Interop;
using System.Runtime.InteropServices;
using System.Collections.Generic;

using STATSTG = Microsoft.VisualStudio.OLE.Interop.STATSTG;
using IStream = Microsoft.VisualStudio.OLE.Interop.IStream;
using System.Text;
using System.ComponentModel;

namespace MaxFileViewer.Models
{
    class DefaultContainer : StreamContainer
    {

        public DefaultContainer(string filepath, string streamName)
            : base(filepath, streamName) { }

        public override bool ReadStream(BackgroundWorker bgworker = null, MaxStreamsContainer maxContainer = null)
        {
            if (!base.ReadStream())
                return false;

            try
            {
                var stat = new STATSTG[1];
                stream.Stat(stat, 0);
                StreamSize = stat[0].cbSize.QuadPart;

                while (StreamSize > TotalRead)
                {
                    var chunk = ParseChunk<DataChunk, ContainerChunk>(this, 0);
                    StreamChunks.Add(chunk);

                    if (bgworker != null)
                        bgworker.ReportProgress((int)(100 * TotalRead / StreamSize));
                }
                
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }
    }
}
