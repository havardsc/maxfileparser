﻿using System;
using Microsoft.VisualStudio.OLE.Interop;
using System.Runtime.InteropServices;
using System.Collections.Generic;

using System.Linq;

using STATSTG = Microsoft.VisualStudio.OLE.Interop.STATSTG;
using IStream = Microsoft.VisualStudio.OLE.Interop.IStream;
using System.Text;
using System.ComponentModel;

namespace MaxFileViewer.Models
{
    class DllDirectoryContainer : StreamContainer
    {
        List<DllDirectoryEntry> dllEntries;

        public override string StreamName { get { return "DllDirectory"; } }

        public DllDirectoryContainer(string filepath)
            : base(filepath) { }

        public List<DllDirectoryEntry> DllEntries
        {
            get
            {
                if (dllEntries == null)
                    dllEntries = StreamChunks.Where(i => 
                        i is DllDirectoryEntry).Select(j => 
                            j as DllDirectoryEntry).ToList();

                return dllEntries;
            }
        }

        public override bool ReadStream(BackgroundWorker bgworker = null, MaxStreamsContainer maxContainer = null)
        {
            if (!base.ReadStream())
                return false;

            try
            {
                var stat = new STATSTG[1];
                stream.Stat(stat, 0);
                StreamSize = stat[0].cbSize.QuadPart;

                while (StreamSize > TotalRead)
                {
                    var chunk = ParseChunk<DataChunk, DllDirectoryEntry>(this, 0);
                    StreamChunks.Add(chunk);

                    if (bgworker != null)
                        bgworker.ReportProgress((int)(100 * TotalRead / StreamSize));
                }

                return true;
            }
            catch (Exception e)
            {
                System.Windows.MessageBox.Show(e.ToString());
                Console.WriteLine(e);
                return false;
            }
        }
    }
}
