﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IStream = Microsoft.VisualStudio.OLE.Interop.IStream;

namespace MaxFileViewer.Models
{
    class DllDirectoryEntry : ContainerChunk
    {
        public string Name
        {
            get 
            { 
                return 
                    Children != null && Children.Count > 0 ?  
                    Children[0].DataString :
                    //Encoding.Unicode.GetString(Children[0].Data.ToArray<byte>()) :
                    string.Empty; 
            }
        }

        public string Filename 
        {
            get 
            { 
                return
                    Children != null && Children.Count > 1 ? 
                    Children[1].DataString :
                    //Encoding.Unicode.GetString(Children[1].Data.ToArray<byte>()) :
                    string.Empty; 
            }
        }

        public DllDirectoryEntry(StreamContainer stream, byte[] header, ulong size, ulong headerSize, int depth) 
            : base(stream, header, size, headerSize, depth) { }

        //public override string ToString()
        //{
        //    return Name + "    " + Filename;
        //}
    }
}
