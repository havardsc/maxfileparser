﻿using System;

using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;

using Microsoft.VisualStudio.OLE.Interop;

using IStream = Microsoft.VisualStudio.OLE.Interop.IStream;
using STATSTG_OI = Microsoft.VisualStudio.OLE.Interop.STATSTG;
using FILETIME = System.Runtime.InteropServices.ComTypes.FILETIME;
using STATSTG = System.Runtime.InteropServices.ComTypes.STATSTG;
using System.Collections.Generic;
using System.Text;


namespace MaxFileViewer.Models
{
    class SummaryInformationContainer : StreamContainer
    {
        public override string StreamName { get { return "SummaryInformation"; } }

        public List<SummaryPropertyData> Properties { get; private set; }

        public SummaryInformationContainer(string filepath)
            : base(filepath) { }

        public override bool ReadStream(
            System.ComponentModel.BackgroundWorker bgworker = null, 
            MaxStreamsContainer maxContainer = null)
        {
            try
            {
                var FMTID_SummaryInformation = new Guid("{F29F85E0-4FF9-1068-AB91-08002B27B3D9}");
                //var FMTID_SummaryInformation = new Guid("{D5CDD502-2E9C-101B-9397-08002B2CF9AE}");

                IStorage Is;
                if (StructuredStorageDefinitions.StgOpenStorage(
                    filepath, 
                    null,
                    (int)(StructuredStorageDefinitions.STGM.SHARE_EXCLUSIVE | StructuredStorageDefinitions.STGM.READWRITE), 
                    IntPtr.Zero, 
                    0, 
                    out Is) == 0 && Is != null)
                {
                    StructuredStorageDefinitions.IPropertySetStorage pss;
                    if (StructuredStorageDefinitions.StgCreatePropSetStg(Is, 0, out pss) == 0)
                    {/*
                        int hr;
                        IEnumSTATPROPSETSTG statpropsetenum;
                        var statpropsetstg = new STATPROPSETSTG[1];
                        pss.Enum(out statpropsetenum);
                        uint fetched;

                        do
                        {
                            hr = statpropsetenum.Next(1, statpropsetstg, out fetched);
                            Console.WriteLine(statpropsetstg[0].fmtid);
                            

                        } while (hr == 0);*/
                        
                        StructuredStorageDefinitions.IPropertyStorage ps;
                        pss.Open(
                            ref FMTID_SummaryInformation,
                            (StructuredStorageDefinitions.STGM.SHARE_EXCLUSIVE | StructuredStorageDefinitions.STGM.READ),
                            out ps);
                        if (ps != null)
                        {
                            int count = 0;
                            int hresult;
                            uint fetched;
                            IEnumSTATPROPSTG penum;

                            Properties = new List<SummaryPropertyData>();

                            var statpropstg = new STATPROPSTG[1];
                            if (ps.Enum(out penum) == 0)
                            {
                                do
                                {
                                    count++;
                                    hresult = penum.Next(1, statpropstg, out fetched);
                                    
                                    var propSpec = new StructuredStorageDefinitions.PropertySpec[1];
                                    propSpec[0].kind = StructuredStorageDefinitions.PropertySpecKind.PropId;
                                    propSpec[0].data.propertyId = statpropstg[0].PROPID;
                                    
                                    var propVariant = 
                                        new PropertyVariant[1];

                                    ps.ReadMultiple(1, propSpec, propVariant);
                                    if (propVariant[0].VarType != VarEnum.VT_EMPTY)
                                        Properties.Add(
                                            new SummaryPropertyData(
                                                propSpec[0].DisplayName(typeof(StructuredStorageDefinitions.SumInfoProperty)),
                                                propVariant[0].Value.ToString(),
                                                propVariant[0].VarType));

                                    //Console.WriteLine(propSpec[0].data.propertyId);
                                    //Console.WriteLine(propSpec[0].DisplayName(typeof(StructuredStorageDefinitions.SumInfoProperty)));
                                    //Console.WriteLine(propVariant[0].Value);
                                    Console.WriteLine(propVariant[0].ToString());
                                    
                                    Console.WriteLine("--");
                                    propVariant[0].Clear();
                                } while (hresult == 0);
                                
                                Marshal.FinalReleaseComObject(ps);
                            }
                            Marshal.FinalReleaseComObject(pss);
                        }
                        Marshal.FinalReleaseComObject(Is);
                    }
                }
                
                return true;
            }

            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }

        public class SummaryPropertyData
        {
            public string Name { get; private set; }
            public string Property { get; private set; }
            public VarEnum Type { get; private set; }

            public SummaryPropertyData(string name, string property, VarEnum type)
            {
                Name = name;
                Property = property;
                Type = type;
            }

            public override string ToString()
            {
                return string.Format("{0}: {1}", Name, Property);
            }
        }
    }
}
