﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;

using Microsoft.VisualStudio.OLE.Interop;

using IStream = Microsoft.VisualStudio.OLE.Interop.IStream;
using STATSTG_OI = Microsoft.VisualStudio.OLE.Interop.STATSTG;
using FILETIME = System.Runtime.InteropServices.ComTypes.FILETIME;
using STATSTG = System.Runtime.InteropServices.ComTypes.STATSTG;
using System.Collections.Generic;
using System.Text;

namespace MaxFileViewer.Models
{
    class DocumentSummaryInformationContainer : StreamContainer
    {
        public override string StreamName { get { return "DocumentSummaryInformation"; } }

        public List<PropertyDataSet> Properties { get; private set; }

        public DocumentSummaryInformationContainer(string filepath)
            : base(filepath) { }

        public override bool ReadStream(
            System.ComponentModel.BackgroundWorker bgworker = null, 
            MaxStreamsContainer maxContainer = null)
        {
            try
            {
                var FMTID_DocumentSummaryInformation = new Guid("{D5CDD502-2E9C-101B-9397-08002B2CF9AE}");

                IStorage Is;
                if (StructuredStorageDefinitions.StgOpenStorage(
                    filepath, 
                    null,
                    (int)(StructuredStorageDefinitions.STGM.SHARE_EXCLUSIVE | StructuredStorageDefinitions.STGM.READWRITE), 
                    IntPtr.Zero, 
                    0, 
                    out Is) == 0 && Is != null)
                {
                    StructuredStorageDefinitions.IPropertySetStorage pss;
                    if (StructuredStorageDefinitions.StgCreatePropSetStg(Is, 0, out pss) == 0)
                    {
                        StructuredStorageDefinitions.IPropertyStorage ps;
                        pss.Open(
                            ref FMTID_DocumentSummaryInformation,
                            (StructuredStorageDefinitions.STGM.SHARE_EXCLUSIVE | StructuredStorageDefinitions.STGM.READ),
                            out ps);
                        if (ps != null)
                        {
                            int hresult;
                            uint fetched;
                            IEnumSTATPROPSTG penum;

                            Properties = new List<PropertyDataSet>();
                            var statpropstg = new STATPROPSTG[1];
                            if (ps.Enum(out penum) == 0)
                            {
                                do
                                {
                                    hresult = penum.Next(1, statpropstg, out fetched);
                                    
                                    var propSpec = new StructuredStorageDefinitions.PropertySpec[1];
                                    propSpec[0].kind = StructuredStorageDefinitions.PropertySpecKind.PropId;
                                    propSpec[0].data.propertyId = statpropstg[0].PROPID;
                                    
                                    var propVariant = 
                                        new PropertyVariant[1];

                                    ps.ReadMultiple(1, propSpec, propVariant);

                                    var p = PropVariantGetElementCount(ref propVariant[0]);
                                    if (propVariant[0].VarType == (VarEnum.VT_VARIANT | VarEnum.VT_VECTOR))
                                    {
                                        for (uint i = 0; i < p; i += 2)
                                        {
                                            var pvar = new PropertyVariant();
                                            InitPropVariantFromPropVariantVectorElem(ref propVariant[0], i, out pvar);
                                            var name = pvar.Value;
                                            InitPropVariantFromPropVariantVectorElem(ref propVariant[0], i + 1, out pvar);
                                            Properties.Add(new PropertyDataSet(name.ToString(), (int)pvar.Value));
                                        }
                                    } 
                                    else if (propVariant[0].VarType == (VarEnum.VT_VECTOR | VarEnum.VT_LPSTR))
                                    {
                                        var currentPropertySet = Properties[0];
                                        uint pCount = 0;
                                        do 
                                        {
                                            var pvar = new PropertyVariant();
                                                
                                            for (int i = 0; i < currentPropertySet.Properties.Capacity; i++)
                                            {
                                                InitPropVariantFromPropVariantVectorElem(ref propVariant[0], pCount++, out pvar);
                                                currentPropertySet.Properties.Add(pvar.Value.ToString());
                                            }
                                            var pIndex = Properties.IndexOf(currentPropertySet) + 1;
                                            currentPropertySet = pIndex != Properties.Count  ? Properties[pIndex] : null;
                                            pvar.Clear();
                                        } while (currentPropertySet != null);
                                    }
                                    propVariant[0].Clear();
                                } while (hresult == 0);
                                    
                                Marshal.FinalReleaseComObject(ps);
                            }
                            Marshal.FinalReleaseComObject(pss);
                        }
                        Marshal.FinalReleaseComObject(Is);
                    }
                }
                
                return true;
            }

            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }



        [DllImport("propsys.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.I4)]
        internal static extern int PropVariantGetElementCount([In] ref PropertyVariant propVar);

        [DllImport("propsys.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        internal static extern int InitPropVariantFromPropVariantVectorElem([In] ref PropertyVariant propvarIn, uint iElem, [Out] out PropertyVariant ppropvar);

        public class PropertyDataSet
        {
            public string Header { get; private set; }
            public List<string> Properties { get; private set; }

            public PropertyDataSet(string name, int size)
            {
                Header = name;
                Properties = new List<string>(size);
            }

            public override string ToString()
            {
                return string.Format("{0} Count: {1}", Header, Properties.Count);
            }
        }
    }
}
