﻿using System;
using Microsoft.VisualStudio.OLE.Interop;
using System.Runtime.InteropServices;
using System.Collections.Generic;

using STATSTG = Microsoft.VisualStudio.OLE.Interop.STATSTG;
using IStream = Microsoft.VisualStudio.OLE.Interop.IStream;
using System.Text;
using System.ComponentModel;

namespace MaxFileViewer.Models
{
    class StreamContainer : IDisposable
    {
        protected IStream stream;
        protected IStorage storage;
        protected string filepath;

        public ulong TotalRead { get; protected set; }
        public ulong StreamSize { get; protected set; }

        public virtual string StreamName { get; protected set; }

        public List<DataChunk> StreamChunks { get; protected set; }

        public StreamContainer(string filepath) 
        {
            this.filepath = filepath;
            StreamChunks = new List<DataChunk>();
            TotalRead = 0;
        }

        public StreamContainer(string filepath, string streamName) 
            : this(filepath)
        {
            StreamName = streamName;
        }

        public virtual void Dispose()
        {
            TotalRead = 0;
            if (stream != null)
                Marshal.ReleaseComObject(stream);
            if (storage != null)
                Marshal.ReleaseComObject(storage);
        }

        public virtual bool ReadStream(BackgroundWorker bgworker = null, MaxStreamsContainer maxContainer = null) 
        {
            OpenStream();
            return stream != null;
        }

        protected virtual void OpenStream() 
        {
            if (string.IsNullOrEmpty(StreamName))
                return;

            try
            {
                stream = MaxFileViewer.Models.StructuredStorageUtilities.OpenStream(
                    filepath, StreamName, out storage, false);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            if (stream == null)
                throw new ArgumentNullException("Stream is null");
        }

        public virtual DataChunk ParseChunk<T, K>(StreamContainer stream, int depth) 
            where T : DataChunk
            where K : ContainerChunk
        {
            var header = stream.ReadBytes(2);
            var sizeData = stream.ReadBytes(4);
            var sizeCheck = BitConverter.ToUInt32(sizeData, 0);

            byte msb;
            ulong headerSize;
            byte[] data;
            ulong size;

            if (sizeCheck != 0)
            {
                msb = sizeData[sizeData.Length - 1];
                sizeData[sizeData.Length - 1] = 0;
                var chunkSize = BitConverter.ToInt32(sizeData, 0);
                size = Convert.ToUInt64(chunkSize);
                sizeData[sizeData.Length - 1] = msb;
                headerSize = 6;
                data = sizeData;
            }
            else
            {
                var lSizeData = stream.ReadBytes(8);
                msb = lSizeData[lSizeData.Length - 1];
                lSizeData[lSizeData.Length - 1] = 0;
                size = BitConverter.ToUInt64(lSizeData, 0);
                lSizeData[lSizeData.Length - 1] = msb;
                data = lSizeData;
                headerSize = 10;
            }

            if (msb == 0)
            {
                //Assumes that no data chunk is larger than uint
                var chunkData = stream.ReadBytes((uint)(size - headerSize));
                var chunk = Activator.CreateInstance(
                    typeof(T),
                    new object[] { header, size, headerSize, chunkData, depth }
                    ) as T;
                
                return chunk;
            }
            else
                return Activator.CreateInstance(
                    typeof(K),
                    new object[] { stream, header, size, headerSize, depth }
                ) as K;
        }

        public static Guid ReadGuid(IStream stream, int guidSize,  ref uint total)
        {
            uint dataRead = 0;
            var guidBytes = new byte[guidSize];
            stream.Read(guidBytes, (uint)guidBytes.Length, out dataRead);
            total += dataRead;

            return new Guid(guidBytes);
        }
        static int readBytesCount = 0;
        public byte[] ReadBytes(uint size)
        {
            readBytesCount++;
            uint dataRead = 0;
            var id = new byte[size];
            try
            {
                if (readBytesCount == 326257)
                {
                }
                stream.Read(id, (uint)id.Length, out dataRead);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw new StackOverflowException(e.Message, e.InnerException);
            }
            TotalRead += dataRead;

            return id;
        }

        public static uint ReadUInt(IStream stream, ref uint total)
        {
            uint dataRead = 0;
            var intBytes = new byte[4];
            stream.Read(intBytes, (uint)intBytes.Length, out dataRead);
            total += dataRead;

            return BitConverter.ToUInt32(intBytes, 0);
        }

        public static int ReadInt(IStream stream, ref uint total)
        {
            uint dataRead = 0;
            var intBytes = new byte[4];
            stream.Read(intBytes, (uint)intBytes.Length, out dataRead);
            total += dataRead;

            return BitConverter.ToInt32(intBytes, 0);
        }

        public static int ReadSize(IStream stream, int byteSize, ref uint total)
        {
            uint dataRead = 0;
            var sizeBytes = new byte[byteSize];
            stream.Read(sizeBytes, (uint)sizeBytes.Length, out dataRead);
            total += dataRead;

            return BitConverter.ToInt32(sizeBytes, 0);
        }

        public static string ReadString(IStream stream, int size, ref uint total)
        {
            return ReadString(stream, size, ref total, n => n * 2 + 2);
        }

        public static string ReadString(IStream stream, int size, ref uint total, Func<int, int> sizeCalcMethod)
        {
            uint dataRead = 0;
            var stringBytes = new byte[sizeCalcMethod(size)];
            stream.Read(stringBytes, (uint)stringBytes.Length, out dataRead);
            total += dataRead;

            return Encoding.Unicode.GetString(stringBytes);
        }

        public static string ByteToHexBitFiddle(byte[] bytes)
        {
            char[] c = new char[bytes.Length * 2];
            int b;
            for (int i = 0; i < bytes.Length; i++)
            {
                b = bytes[i] >> 4;
                c[i * 2] = (char)(55 + b + (((b - 10) >> 31) & -7));
                b = bytes[i] & 0xF;
                c[i * 2 + 1] = (char)(55 + b + (((b - 10) >> 31) & -7));
            }
            return new string(c);
        }
    }
}
