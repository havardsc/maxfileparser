﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IStream = Microsoft.VisualStudio.OLE.Interop.IStream;

namespace MaxFileViewer.Models
{
    class ClassDataEntry : ContainerChunk
    {
        public uint[] ClassID
        {
            get
            {
                return Children.Count > 1 ? 
                    new uint[]
                    {
                        BitConverter.ToUInt32(Children[0].Data.Skip(0).Take(4).ToArray(), 0),
                        BitConverter.ToUInt32(Children[0].Data.Skip(4).Take(4).ToArray(), 0)
                    } :
                    null;
            }
        }

        public uint SuperClassID
        {
            get 
            { 
                return Children.Count > 0 ?
                    BitConverter.ToUInt32(Children[0].Data.Skip(8).Take(4).ToArray(), 0) :
                    uint.MaxValue; 
            }
        }

        public ClassDataEntry(StreamContainer stream, byte[] header, ulong size, ulong headerSize, int depth)
            : base(stream, header, size, headerSize, depth) { }

        //public override string ToString()
        //{
        //    return
        //        String.Format(
        //        "ClassID:#({1}) SuperclassID:{1}",
        //        ClassID[1].ToString("X4") + ", " + ClassID[0].ToString("X4"),
        //        SuperClassID);
        //}   
    }
}
