﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MaxFileViewer.Models
{
    class MaxStreamsContainer : IDisposable
    {
        public Dictionary<string, StreamContainer> RetrievedStreams { get; private set; }
        public string Filepath { get; private set; }
        public List<string> AvailableStreams { get; private set; }

        public delegate void StreamsChangedEventHandler(object sender, params object[] arg);
        public event StreamsChangedEventHandler StreamsChanged;

        public MaxStreamsContainer(string filepath)
        {
            Filepath = filepath;
            AvailableStreams = StructuredStorageUtilities.GetStreams(filepath);
            RetrievedStreams = new Dictionary<string, StreamContainer>();
        }

        public StreamContainer GetStream(string streamName, bool reload)
        {
            if (!AvailableStreams.Contains(streamName))
                return null;

            StreamContainer container;

            if (streamName.Contains("ClassDirectory"))
                container = SetContainer<ClassDirectoryContainer>(streamName, reload);
            else if (streamName.Contains("DllDirectory"))
                container = SetContainer<DllDirectoryContainer>(streamName, reload);
            else if (streamName.Contains("ClassData"))
                container = SetContainer<ClassDataContainer>(streamName, reload);
            else if (streamName.Contains("Scene"))
                container = SetContainer<SceneContainer>(streamName, reload);
            else if (streamName.Contains("FileAssetMetaData"))
                container = SetContainer<FileAssetMetaDataContainer>(streamName, reload);
            else if (streamName.Contains("DocumentSummaryInformation"))
                container = SetContainer<DocumentSummaryInformationContainer>(streamName, reload);
            else if (streamName.Contains("SummaryInformation"))
                container = SetContainer<SummaryInformationContainer>(streamName, reload);
            else
                container = SetContainer<DefaultContainer>(streamName, reload, true);

            return container;
        }

        public void Dispose()
        {
            foreach (var container in RetrievedStreams)
                container.Value.Dispose();
        }

        StreamContainer SetContainer<TContainer>(string streamName, bool reload, bool useStringName = false) 
            where TContainer : StreamContainer
        {
            StreamContainer container; 
            if (RetrievedStreams.Keys.Contains(streamName) && !reload)
                container = RetrievedStreams[streamName];
            else if (!useStringName)
            {
                container = Activator.CreateInstance(
                    typeof(TContainer),
                    new object[] { Filepath }) as TContainer;

                RetrievedStreams[streamName] = container;
            }
            else
            {
                container = Activator.CreateInstance(
                    typeof(TContainer),
                    new object[] { Filepath, streamName }) as TContainer;

                RetrievedStreams[streamName] = container;
            }
            Dispose();
            return container;
        }
    }
}
