﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MaxFileViewer.Models;
using MaxFileViewer.ViewModels.Utilities;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace MaxFileViewer.ViewModels
{
    class FileAssetsMetaDataViewModel : DefaultStreamViewModel
    {
        FileAssetMetaDataContainer assetContainer;
        FileAssetMetaData selectedItem;

        public static new string DisplayName { get { return "File Assets"; } }

        public List<FileAssetMetaData> Assets { get; private set; }

        public FileAssetMetaData SelectedItem
        {
            get { return selectedItem; }
            set
            {
                if (value == selectedItem) return;
                selectedItem = value;
                OnPropertyChanged("SelectedItem");
            }
        }

        public FileAssetsMetaDataViewModel(StreamContainer container)
            : base(container, DisplayName)
        {
            GetAssets();
        }

        void GetAssets()
        {
            assetContainer = container as FileAssetMetaDataContainer;
            Assets = assetContainer.Assets;
        }
    }
}
