﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;

using MaxFileViewer.Models;
using MaxFileViewer.ViewModels.Utilities;
using Resources = MaxFileViewer.Properties.Resources;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace MaxFileViewer.ViewModels
{
    class MainViewModel : ViewModelBase
    {
        static MainViewModel appInstance;
        public static MainViewModel AppInstance
        {
            get
            {
                if (appInstance == null)
                    appInstance = new MainViewModel();
                
                return appInstance;
            }
        }

        ObservableCollection<MaxFileViewerItemViewModel> maxFiles;
        MaxFileViewerItemViewModel selectedMaxFile;

        public CommandBaseViewModel OpenFileCommand { get; private set; }
        
        public ObservableCollection<MaxFileViewerItemViewModel> MaxFiles { get { return maxFiles; } }
        public MaxFileViewerItemViewModel SelectedMaxFile
        {
            get { return selectedMaxFile; }
            set
            {
                if (value == selectedMaxFile) return;
                selectedMaxFile = value;
                OnPropertyChanged("SelectedMaxFile");
            }
        }

        public MainViewModel()
            : base()
        {
            maxFiles = new ObservableCollection<MaxFileViewerItemViewModel>();

            OpenFileCommand = new CommandBaseViewModel(
                Resources.OpenMaxFileString, new RelayCommand<object>(OpenFile));
        }

        void OpenFile(object _)
        {
            var dialog = new System.Windows.Forms.OpenFileDialog();
            dialog.Filter = "3ds Max files (*.max)|*.max|All files (*.*)|*.*";
            dialog.CheckFileExists = true;

            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                LoadnewFile(dialog.FileName);
        }

        void LoadnewFile(string filename)
        {
            var newFile = new MaxFileViewerItemViewModel(filename);
            maxFiles.Add(newFile);
            SelectedMaxFile = newFile;
        }
    }
}
 