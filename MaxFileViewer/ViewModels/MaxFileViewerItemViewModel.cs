﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using MaxFileViewer.Models;
using MaxFileViewer.ViewModels.Utilities;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace MaxFileViewer.ViewModels
{
    class MaxFileViewerItemViewModel : ItemViewModel
    {
        string filepath;
        StreamsSelectionViewModel streamsSelectionViewModel;
        MaxStreamsContainer streamsContainer;
        ObservableCollection<DefaultStreamViewModel> openItems;
        DefaultStreamViewModel selectedItem;

        public ObservableCollection<DefaultStreamViewModel> OpenItems 
        { 
            get { return openItems; }
            set
            {
                if (value == openItems) return;
                openItems = value;
                OnPropertyChanged("OpenItems");
            }
        }

        public DefaultStreamViewModel SelectedItem
        {
            get { return selectedItem; }
            set
            {
                if (value == selectedItem) return;
                selectedItem = value;
                OnPropertyChanged("SelectedItem");
            }
        }

        public MaxStreamsContainer MaxStreamsContainer { get { return streamsContainer; } }

        public StreamsSelectionViewModel StreamsSelectionViewModel
        {
            get { return streamsSelectionViewModel; }
        }

        public MaxFileViewerItemViewModel(string path)
            : base(Path.GetFileName(path))
        {
            filepath = path;
            streamsContainer = new MaxStreamsContainer(path);
            openItems = new ObservableCollection<DefaultStreamViewModel>();
            streamsSelectionViewModel = new StreamsSelectionViewModel(this);
        }

        public override string ToString()
        {
            return DisplayName + base.ToString();
        }
    }
}
