﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MaxFileViewer.Models;
using MaxFileViewer.ViewModels.Utilities;
using System.Collections.ObjectModel;

namespace MaxFileViewer.ViewModels
{
    class SceneEntryPropertiesViewModel : ViewModelBase
    {
        public SceneEntry SceneEntry { get; private set; }

        public string NodeName 
        { 
            get 
            { 
                if (SceneEntry.NodeName != null)
                    return SceneEntry.NodeName;
                return null;
            } 
        }

        public string ObjectChunkName
        {
            get 
            { 
                if (SceneEntry.ObjectChunk != null)
                    return SceneEntry.ObjectChunk.ID + " " + SceneEntry.ObjectChunk.Class.Description;

                return null;
            }
        }
        
        public string MaterialChunkName 
        {
            get 
            { 
                if (SceneEntry.MaterialChunk != null)
                    return SceneEntry.MaterialChunk.ID + " " + SceneEntry.MaterialChunk.Class.Description;

                return null;
            } 
        }

        public string ControllerChunkName
        {
            get 
            { 
                if (SceneEntry.ControllerChunk != null)
                    return SceneEntry.ControllerChunk.ID + " " + SceneEntry.ControllerChunk.Class.Description;

                return null;
            }
        }

        public string LayerChunkName
        {
            get 
            { 
                if (SceneEntry.LayerChunk != null)
                    return SceneEntry.LayerChunk.ID + " " + SceneEntry.LayerChunk.Class.Description;

                return null;
            } 
        }

        public List<string> AllReferences
        {
            get
            {
                if (SceneEntry.References != null)
                    return SceneEntry.References.Select(i => i.ToString()).ToList();

                return null;
            }
        }

        public SceneEntryPropertiesViewModel(SceneEntry sceneEntry) 
            : base()
        {
            SceneEntry = sceneEntry;
        }
    }
}
