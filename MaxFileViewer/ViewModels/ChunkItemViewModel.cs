﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MaxFileViewer.Models;
using MaxFileViewer.ViewModels.Utilities;
using System.Collections.ObjectModel;

namespace MaxFileViewer.ViewModels
{
    class ChunkItemViewModel : LazyTreeViewItem
    {
        SceneViewModel sceneViewModel;

        protected DataChunk dataChunk;

        public ChunkItemViewModel(DataChunk dataChunk, SceneViewModel sceneViewModel) 
            : base()
        {
            this.sceneViewModel = sceneViewModel;
            isDummy = false;
            if (dataChunk is ContainerChunk)
            {
                this.dataChunk = dataChunk as ContainerChunk;
                children = new ObservableCollection<LazyTreeViewItem>();
                Children.Add(DummyChild);
            }
            else
                this.dataChunk = dataChunk;
            
        }

        public override bool IsSelected
        {
            get { return isSelected; }
            set
            {
                if (isSelected == value) return;
                isSelected = value;
                if (isSelected && dataChunk is SceneEntry)
                    sceneViewModel.SetSelectedDataChunk(this, dataChunk as SceneEntry);
                OnPropertyChanged("IsSelected");
            }
        }

        protected override void LoadChildren()
        {
            if (!isExpanded)
            {
                children.Clear();
                Children.Add(DummyChild);
                return;
            }

            children.Remove(DummyChild);

            var containerChunk = dataChunk as ContainerChunk;

            foreach (var child in containerChunk.Children)
                children.Add(new ChunkItemViewModel(child, sceneViewModel));

            OnPropertyChanged("Children");
        }

        public override string DisplayName { get { return dataChunk.ToString(); } }
    }
}
