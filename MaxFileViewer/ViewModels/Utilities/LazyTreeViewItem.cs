﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MaxFileViewer.Models;
using MaxFileViewer.ViewModels.Utilities;
using System.Collections.ObjectModel;

namespace MaxFileViewer.ViewModels
{
    class LazyTreeViewItem : ViewModelBase
    {
        static protected readonly LazyTreeViewItem DummyChild = new LazyTreeViewItem(true);

        protected bool isExpanded, isSelected, isDummy;
        protected ObservableCollection<LazyTreeViewItem> children;
        

        public virtual bool HasDummyChild
        {
            get { return !isDummy && Children != null && Children.Count == 1 && Children[0] == DummyChild; }
        }

        public ObservableCollection<LazyTreeViewItem> Children
        {
            get { return children; }
            set
            {
                children = value;
                OnPropertyChanged("Children");
            }
        }

        public virtual bool IsExpanded
        {
            get { return isExpanded; }
            set
            {
                if (isExpanded == value) return;
                isExpanded = value;
                OnPropertyChanged("IsExpanded");

                if (!HasDummyChild && isExpanded) return;

                LoadChildren();
            }
        }

        public virtual bool IsSelected
        {
            get { return isSelected; }
            set
            {
                if (isSelected == value) return;
                isSelected = value;
                OnPropertyChanged("IsSelected");
            }
        }

        LazyTreeViewItem(bool isDummy) { this.isDummy = isDummy; }
        public LazyTreeViewItem() { }
        
        protected virtual void LoadChildren()
        {
            if (!isExpanded)
            {
                children.Clear();
                Children.Add(DummyChild);
                return;
            }

            children.Remove(DummyChild);
        }
    }
}
