﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MaxFileViewer.ViewModels.Utilities
{
    class ItemViewModel : ViewModelBase
    {
        string name;

        public ItemViewModel(string displayName) 
            : base()
        {
            DisplayName = displayName;
        }

        public string Name
        {
            get { return name; }
            set
            {
                if (name == value) return;
                name = value;
                OnPropertyChanged("Name");
            }
        }
    }
}
