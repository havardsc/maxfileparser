﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace MaxFileViewer.ViewModels.Utilities
{
    class CommandBaseViewModel : ViewModelBase
    {
        public ICommand Command { get; private set; }
        public bool CanExecute
        {
            get { return Command.CanExecute(null); }
        }

        public CommandBaseViewModel(string displayName, ICommand command)
        {
            if (command == null)
                throw new ArgumentNullException("command");

            DisplayName = displayName;
            Command = command;
        }
    }
}
