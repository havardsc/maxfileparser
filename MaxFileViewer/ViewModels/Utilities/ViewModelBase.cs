﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace MaxFileViewer.ViewModels.Utilities
{
    abstract class ViewModelBase : INotifyPropertyChanged, IDisposable
    {
        protected string displayName;

        public virtual string DisplayName { get; protected set; }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                var e = new PropertyChangedEventArgs(propertyName);
                handler(this, e);
            }
        }

        public void Dispose()
        {
            OnDispose();
        }

        protected virtual void OnDispose() { }

        protected virtual bool ThrowOnInvalidPropertyName { get; private set; }
    }
}
