﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace MaxFileViewer.ViewModels.Utilities.Converters
{
    [ValueConversion(typeof(uint[]), typeof(string))]
    public class ClassIdConverter : BaseConverter, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            uint[] superClass = value as uint[];
            return superClass[0] + ", " + superClass[1];
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}
