﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MaxFileViewer.Models;

namespace MaxFileViewer.ViewModels
{
    class ClassDirectoyViewModel : DefaultStreamViewModel
    {
        public static new string DisplayName { get { return "Class Directory"; } }

        public List<ClassDirectoryEntry> Entries { get; private set; }
        

        public ClassDirectoyViewModel(StreamContainer container)
            : base(container, DisplayName)
        {
            CreateEntriesList();
        }

        void CreateEntriesList()
        {
            Entries = container.StreamChunks.Select(x => x as ClassDirectoryEntry).ToList();
        }
    }
}
