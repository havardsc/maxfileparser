﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MaxFileViewer.Models;
using MaxFileViewer.ViewModels.Utilities;
using System.Collections.ObjectModel;
using System.Windows.Input;


namespace MaxFileViewer.ViewModels
{
    class DllDirectoryViewModel : DefaultStreamViewModel
    {
        public static new string DisplayName { get { return "Dll Directory"; } }

        public List<DllDirectoryEntry> DllItems { get; private set; }

        public DllDirectoryViewModel(StreamContainer container)
            : base(container, DisplayName)
        {
            CreateDLLlist();
        }

        void CreateDLLlist()
        {
            DllItems = container.StreamChunks.Skip(1).Select(x => x as DllDirectoryEntry).ToList();
        }
    }
}
