﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows;
using System.Collections;

namespace MaxFileViewer.ViewModels.Behaviors
{
    static class PersistentTabBehavior
    {
        static readonly Dictionary<TabControl, PersistentTabItemsSourceHandler> ItemSourceHandlers = 
            new Dictionary<TabControl, PersistentTabItemsSourceHandler>();
    
        public static readonly DependencyProperty ItemsSourceProperty =
            DependencyProperty.RegisterAttached(
            "ItemsSource", typeof(IEnumerable), typeof(PersistentTabBehavior),
            new UIPropertyMetadata(null, OnItemsSourcePropertyChanged)
        );
    
        public static void SetItemsSource(DependencyObject tab, IEnumerable source)
        {
            tab.SetValue(ItemsSourceProperty, source);
        }
    
        public static object GetItemsSource(DependencyObject tab)
        {
            return tab.GetValue(ItemsSourceProperty);
        }
    
        static void OnItemsSourcePropertyChanged(
            DependencyObject dpo, DependencyPropertyChangedEventArgs e)
        {
            var tabControl = dpo as TabControl;
    
            if (tabControl == null)
                return;
    
            if (tabControl.ItemsSource != null)
                return;
    
            PersistentTabItemsSourceHandler handler;
    
            if (!ItemSourceHandlers.ContainsKey(tabControl))
            {
                handler = new PersistentTabItemsSourceHandler(tabControl);
                ItemSourceHandlers.Add(tabControl, handler);
                //tabControl.Unloaded += ItemsSourceTabControlUnloaded;
            }
            else
            {
                handler = ItemSourceHandlers[tabControl];
            }

            if (tabControl.IsLoaded)
                handler.Load();
        }
    
        static void ItemsSourceTabControlUnloaded(object sender, RoutedEventArgs e)
        {
            var tabControl = sender as TabControl;
    
            if (tabControl == null)
                return;
    
            RemoveFromItemSourceHandlers(tabControl);
            tabControl.Unloaded -= ItemsSourceTabControlUnloaded;
        }
    
        static void RemoveFromItemSourceHandlers(TabControl tabControl)
        {
            if (!ItemSourceHandlers.ContainsKey(tabControl))
                return;
    
            ItemSourceHandlers[tabControl].Dispose();
            ItemSourceHandlers.Remove(tabControl);
        }

        static readonly Dictionary<TabControl, PersistentTabSelectedItemHandler> SelectedItemHandlers =
            new Dictionary<TabControl, PersistentTabSelectedItemHandler>();
    
        // FrameworkPropertyMetadata is required for TwoWay Binding to work
        public static readonly DependencyProperty SelectedItemProperty =
            DependencyProperty.RegisterAttached(
            "SelectedItem", typeof(object), typeof(PersistentTabBehavior),
            new FrameworkPropertyMetadata(
                null,
                FrameworkPropertyMetadataOptions.BindsTwoWayByDefault,
                OnSelectedItemPropertyChanged));
    
        public static void SetSelectedItem(DependencyObject tab, object source)
        {
            tab.SetValue(SelectedItemProperty, source);
        }
    
        public static object GetSelectedItem(DependencyObject tab)
        {
            return tab.GetValue(SelectedItemProperty);
        }
    
        static void OnSelectedItemPropertyChanged(DependencyObject dpo, DependencyPropertyChangedEventArgs e)
        {
            var tabControl = dpo as TabControl;
    
            if (tabControl == null)
                return;
    
            if (tabControl.ItemsSource != null)
                return;
    
            PersistentTabSelectedItemHandler handler;
    
            if (!SelectedItemHandlers.ContainsKey(tabControl))
            {
                handler = new PersistentTabSelectedItemHandler(tabControl);
                SelectedItemHandlers.Add(tabControl, handler);
                tabControl.Unloaded += SelectedItemTabControlUnloaded;
            }
            else
            {
                handler = SelectedItemHandlers[tabControl];
            }
    
            handler.ChangeSelectionFromProperty();
        }
    
        static void SelectedItemTabControlUnloaded(object sender, RoutedEventArgs e)
        {
            var tabControl = sender as TabControl;
    
            if (tabControl == null)
                return;
    
            RemoveFromSelectedItemHandlers(tabControl);
    
            tabControl.Unloaded -= SelectedItemTabControlUnloaded;
        }
    
        static void RemoveFromSelectedItemHandlers(TabControl tabControl)
        {
            if (!SelectedItemHandlers.ContainsKey(tabControl))
                return;
    
            SelectedItemHandlers[tabControl].Dispose();
            SelectedItemHandlers.Remove(tabControl);
        }
    }
}
