﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Collections;

namespace MaxFileViewer.ViewModels.Behaviors
{
    class PersistentTabSelectedItemHandler
    {
        public TabControl Tab { get; private set; }
     
        public PersistentTabSelectedItemHandler(TabControl tab)
        {
            Tab = tab;
            Tab.SelectionChanged += ChangeSelectionFromUi;
        }
     
        public void Dispose()
        {
            Tab.SelectionChanged -= ChangeSelectionFromUi;
            Tab = null;
        }
     
         public void ChangeSelectionFromProperty()
         {
             var selectedObject = Tab.GetValue(PersistentTabBehavior.SelectedItemProperty);
     
             if (selectedObject == null)
             {
                 Tab.SelectedItem = null;
                 return;
             }
     
             foreach (TabItem tabItem in Tab.Items)
             {
                 if (tabItem.DataContext == selectedObject)
                 {
                     if (!tabItem.IsSelected)
                         tabItem.IsSelected = true;
     
                     break;
                 }
             }
         }
     
         void ChangeSelectionFromUi(object sender, SelectionChangedEventArgs e)
         {
             if (e.AddedItems.Count >= 1)
             {
                 var selectedObject = e.AddedItems[0];
     
                 var selectedItem = selectedObject as TabItem;
     
                 if (selectedItem != null)
                     SelectedItemProperty(selectedItem);
             }
         }
     
         void SelectedItemProperty(TabItem selectedTabItem)
         {
             var tabObjects = Tab.GetValue(PersistentTabBehavior.ItemsSourceProperty) as IEnumerable;
     
             if (tabObjects == null)
                 return;
     
             foreach (var tabObject in tabObjects)
             {
                 if (tabObject == selectedTabItem.DataContext)
                 {
                     //Tab.ContentTemplate = Tab.ContentTemplateSelector.SelectTemplate(selectedTabItem.DataContext, Tab);
                     PersistentTabBehavior.SetSelectedItem(Tab, tabObject);
                     return;
                 }
             }
         }
    }
}
