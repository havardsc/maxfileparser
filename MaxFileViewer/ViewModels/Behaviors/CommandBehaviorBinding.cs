﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Reflection;
using System.Windows;

namespace MaxFileViewer.ViewModels.Behaviors
{
    class CommandBehaviorBinding : IDisposable
    {
        public DependencyObject Owner { get; private set; }
        public ICommand Command { get; set; }
        public object CommandParameter { get; set; }
        public string EventName { get; private set; }
        public EventInfo Event { get; private set; }
        public Delegate EventHandler { get; private set; }

        public void BindEvent(DependencyObject owner, string eventName)
        {
            EventName = eventName;
            Owner = owner;
            Event = Owner.GetType().GetEvent(EventName, BindingFlags.Public | BindingFlags.Instance);
            if (Event == null)
                throw new InvalidOperationException(String.Format("Could not resolve event name {0}", EventName));

            //Create an event handler for the event that will call the ExecuteCommand method
            EventHandler = EventHandlerGenerator.CreateDelegate(
                Event.EventHandlerType, typeof(CommandBehaviorBinding).GetMethod("ExecuteCommand", BindingFlags.Public | BindingFlags.Instance), this);
            //Register the handler to the Event
            Event.AddEventHandler(Owner, EventHandler);
        }

        public void ExecuteCommand()
        {
            if (Command.CanExecute(CommandParameter))
                Command.Execute(CommandParameter);
        }

        bool disposed = false;

        public void Dispose()
        {
            if (!disposed)
            {
                Event.RemoveEventHandler(Owner, EventHandler);
                disposed = true;
            }
        }
    }
}
