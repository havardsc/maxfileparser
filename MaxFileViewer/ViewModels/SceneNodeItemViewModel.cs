﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MaxFileViewer.Models;
using MaxFileViewer.ViewModels.Utilities;
using System.Collections.ObjectModel;

namespace MaxFileViewer.ViewModels
{
    class SceneNodeItemViewModel : LazyTreeViewItem
	{
        SceneViewModel sceneViewModel;
        bool doLoadChildren;

        public SceneEntry SceneEntry { get; private set; }

        public override string DisplayName
        {
            get
            {
                return SceneEntry.ID + " " + SceneEntry.NodeName;
            }
            protected set
            {
                base.DisplayName = value;
            }
        }

        public SceneNodeItemViewModel(DataChunk dataChunk, SceneViewModel sceneViewModel, bool doLoadChildren = true) 
            : base() 
        {
            this.doLoadChildren = doLoadChildren;
            this.sceneViewModel = sceneViewModel;
            SceneEntry = dataChunk as SceneEntry;
            isDummy = false;
            if (doLoadChildren && SceneEntry.SceneEntryChildren != null && 
                SceneEntry.SceneEntryChildren.Count > 0)
            {
                children = new ObservableCollection<LazyTreeViewItem>();
                Children.Add(DummyChild);
            }
        }

        public SceneNodeItemViewModel FindByID(int id)
        {
            SceneNodeItemViewModel model = null;

            if (!HasDummyChild && children != null)
                foreach (SceneNodeItemViewModel child in children)
                {
                    if (child.SceneEntry.ID == id)
                        return child;
                    model = child.FindByID(id);
                    if (model != null) 
                        break;
                }

            return model;
        }

        public override bool IsSelected
        {
            get { return isSelected; }
            set
            {
                if (isSelected == value) return;
                isSelected = value;
                if (isSelected)
                    sceneViewModel.SetSelectedDataChunk(this, SceneEntry);
                OnPropertyChanged("IsSelected");
            }
        }

        protected override void LoadChildren()
        {
            if (!doLoadChildren)
                return;
            if (!isExpanded)
            {
                children.Clear();
                Children.Add(DummyChild);
                return;
            }

            children.Remove(DummyChild);

            foreach (var child in SceneEntry.SceneEntryChildren)
                if (child is SceneEntry)
                    children.Add(new SceneNodeItemViewModel(child, sceneViewModel));

            OnPropertyChanged("Children");
        }
	}
}
