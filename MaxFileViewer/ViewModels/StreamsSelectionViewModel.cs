﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

using MaxFileViewer.Models;
using MaxFileViewer.ViewModels.Utilities;
using System.Windows.Input;
using System.ComponentModel;

namespace MaxFileViewer.ViewModels
{
    class StreamsSelectionViewModel : Utilities.ViewModelBase
    {
        MaxStreamsContainer container;
        ObservableCollection<StreamAndStatus> streams;
        MaxFileViewerItemViewModel parent;
        StreamAndStatus selectedItem;
        StreamContainer currentStreamContainer;
        StreamAndStatus currentStreamAndStatus;
        BackgroundWorker streamContainerLoadWorker;
        List<DefaultStreamViewModel> loadedViewModels;

        public ObservableCollection<StreamAndStatus> Streams 
        {
            get { return streams; }
            set
            {
                streams = value;
                OnPropertyChanged("Streams");
            }
        }

        public StreamAndStatus SelectedItem
        {
            get { return selectedItem; }
            set
            {
                if (selectedItem == value) return;
                selectedItem = value;
                OnPropertyChanged("SelectedItem");
            }
        }

        public ICommand MouseDoubleClickCommand { get; private set; }

        public StreamsSelectionViewModel(MaxFileViewerItemViewModel parent)
        {
            this.parent = parent;
            this.container = parent.MaxStreamsContainer;
            this.container.StreamsChanged += StreamsChanged;

            loadedViewModels = new List<DefaultStreamViewModel>();

            streams = new ObservableCollection<StreamAndStatus>(
                container.AvailableStreams.Select
                (i => new StreamAndStatus(i)).ToList());

            streamContainerLoadWorker = new BackgroundWorker();
            streamContainerLoadWorker.WorkerReportsProgress = true;
            streamContainerLoadWorker.ProgressChanged += new ProgressChangedEventHandler(StreamProgressChanged);
            streamContainerLoadWorker.DoWork += new DoWorkEventHandler(LoadStream);
            streamContainerLoadWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(StreamLoaded);
            MouseDoubleClickCommand = new RelayCommand<object>(MouseDoubleClick);
        }

        DefaultStreamViewModel SelectViewModel(StreamContainer container, out bool isLoaded)
        {
            if (container.StreamName.Contains("DllDirectory"))
                return IsModelLoaded<DllDirectoryViewModel>(container, out isLoaded);
            else if (container.StreamName.Contains("Scene"))
                return IsModelLoaded<SceneViewModel>(container, out isLoaded);
            else if (container.StreamName.Contains("FileAssetMetaData"))
                return IsModelLoaded<FileAssetsMetaDataViewModel>(container, out isLoaded);
            else if (container.StreamName.Contains("DocumentSummaryInformation"))
                return IsModelLoaded<DocumentSummaryInformationViewModel>(container, out isLoaded);
            else if (container.StreamName.Contains("SummaryInformation"))
                return IsModelLoaded<SummaryInformationViewModel>(container, out isLoaded);
            else if (container.StreamName.Contains("ClassDirectory"))
                return IsModelLoaded<ClassDirectoyViewModel>(container, out isLoaded);

            return IsModelLoaded<DefaultStreamViewModel>(container, out isLoaded);//new DefaultStreamViewModel(container, container.StreamName);
        }

        DefaultStreamViewModel IsModelLoaded<Type>(StreamContainer container, out bool isLoaded) 
            where Type : DefaultStreamViewModel
        {
            DefaultStreamViewModel viewModel;
            //if (typeof(Type) == typeof(DefaultStreamViewModel))
            //    viewModel = loadedViewModels.FirstOrDefault(i => i.DisplayName == container.StreamName);
            //else
            //    viewModel = loadedViewModels.FirstOrDefault(i => i is Type);

            //if (viewModel == null)
            //{
                //isLoaded = false;
            isLoaded = false;
            if (typeof(Type) == typeof(DefaultStreamViewModel))
                viewModel = new DefaultStreamViewModel(container, container.StreamName);
            else
                viewModel = Activator.CreateInstance(typeof(Type), new object[] { container }) as Type;
            loadedViewModels.Add(viewModel);
            return viewModel;
            //}
            //isLoaded = true;
            //return viewModel;
        }

        void StreamProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            currentStreamAndStatus.Percentage = e.ProgressPercentage;
        }

        void StreamLoaded(object sender, AsyncCompletedEventArgs e)
        {
            StreamsChanged(null, "Loaded", "Available");
            bool isLoaded;
            var streamView = SelectViewModel(currentStreamContainer, out isLoaded);

            if (!isLoaded)
                parent.OpenItems.Add(streamView);

            parent.SelectedItem = streamView;
            currentStreamContainer = null;
            currentStreamAndStatus = null;
        }

        void LoadStream(object sender, DoWorkEventArgs e)
        {
            var args = e.Argument as object[];
            currentStreamContainer = args[0] as StreamContainer;
            currentStreamAndStatus = args[1] as StreamAndStatus;
            
            currentStreamContainer.ReadStream(streamContainerLoadWorker, container);
            parent.Dispose();
        }

        void MouseDoubleClick(object _)
        {
            if (SelectedItem == null || streamContainerLoadWorker.IsBusy) 
                return;

            var loadedViewModel = loadedViewModels.FirstOrDefault(i => SelectedItem.Stream.Contains(i.StreamContainerName));
            if (loadedViewModel != null)
            {
                parent.SelectedItem = loadedViewModel;
                return;
            }
            var streamContainer = container.GetStream(SelectedItem.Stream, false);
            StreamsChanged(null, "Loading", "Unavailable");
            streamContainerLoadWorker.RunWorkerAsync(new object[] {streamContainer, SelectedItem});
        }

        void StreamsChanged(object sender, params object[] arg)
        {
            for (int i = 0; i < container.AvailableStreams.Count; i++)
            {
                if (container.RetrievedStreams.ContainsKey(container.AvailableStreams[i]))
                    streams[i].Status = arg[0].ToString();
                else if (arg.Length > 1)
                    streams[i].Status = arg[1].ToString();
            }
            OnPropertyChanged("Streams");
        }

        internal class StreamAndStatus : ViewModelBase
        {
            string status;
            int percentage;
            public string Stream { get; private set; }
            public string Status 
            {
                get { return status; }
                set
                {
                    if (value == status) return;
                    status = value;
                    if (status == "Loaded")
                        Percentage = 100;
                    OnPropertyChanged("Status");
                }
            }
            public int Percentage
            {
                get { return percentage; }
                set
                {
                    if (value == percentage) return;
                    percentage = value;
                    OnPropertyChanged("Percentage");
                }
            }


            public StreamAndStatus(string stream) 
            { 
                Stream = stream;
                status = "Available";
                percentage = 0;
            }
        }
    }
}
