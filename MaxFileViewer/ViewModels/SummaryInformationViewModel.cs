﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MaxFileViewer.Models;

namespace MaxFileViewer.ViewModels
{
    class SummaryInformationViewModel : DefaultStreamViewModel
    {
        SummaryInformationContainer summaryContainer;

        public static new string DisplayName { get { return "Summary Information"; } }

        public List<SummaryInformationContainer.SummaryPropertyData> Properties 
        { get { return summaryContainer.Properties; } }

        public SummaryInformationViewModel(StreamContainer container)
            : base(container, DisplayName)
        {
            summaryContainer = container as SummaryInformationContainer;
        }
    }
}
