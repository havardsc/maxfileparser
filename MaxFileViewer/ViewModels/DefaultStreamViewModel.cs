﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MaxFileViewer.Models;
using MaxFileViewer.ViewModels.Utilities;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace MaxFileViewer.ViewModels
{
    class DefaultStreamViewModel : ItemViewModel
    {
        protected StreamContainer container;

        public List<ChunkItemViewModel> DataChunks { get; protected set; }
        public string StreamContainerName { get { return container.StreamName; } }

        public DefaultStreamViewModel(StreamContainer container, string displayName) 
            : base(displayName)
        {
            this.container = container;
            CreateDataChunks();
        }

        public virtual void SelectedDataChunkChanged(object sender, object dataChunk) { }

        protected virtual void CreateDataChunks() 
        {
            DataChunks = container.StreamChunks.Select(i => new ChunkItemViewModel(i, null)).ToList();
        }
    }
}
