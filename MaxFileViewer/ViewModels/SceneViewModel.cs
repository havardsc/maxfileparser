﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MaxFileViewer.Models;
using MaxFileViewer.ViewModels.Utilities;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace MaxFileViewer.ViewModels
{
    class SceneViewModel : DefaultStreamViewModel
    {
        SceneContainer sceneContainer;
        ContainerChunk cc;
        SceneEntryPropertiesViewModel selectedSceneEntry;

        public static new string DisplayName { get { return "Scene"; } }

        public List<SceneNodeItemViewModel> SceneGraphItems { get; private set; }
        public List<LayerItemViewModel> Layers { get; private set; }
        public SceneEntryPropertiesViewModel SelectedSceneEntry
        {
            get { return selectedSceneEntry; }
            set
            {
                if (value == selectedSceneEntry) return;
                selectedSceneEntry = value;
                OnPropertyChanged("SelectedSceneEntry");
            }
        }

        public SceneViewModel(StreamContainer container)
            : base(container, DisplayName)
        {
            sceneContainer = container as SceneContainer;
            cc = sceneContainer.RootNode;
            CreateDataChunks();
            CreateSceneGraph();
            CreateLayerGraph();
        }

        public void SetSelectedDataChunk(ViewModelBase sender, SceneEntry entry)
        {
            var sceneGraphItem = SceneGraphItems[0].FindByID(entry.ID);
            if (sceneGraphItem != null)
                sceneGraphItem.IsSelected = true;

            var refChunk = entry.Children.FirstOrDefault(i => SceneEntry.CompareHeaders(i.Header, new byte[] { 52, 32 }));// || CompareHeaders(i.Header, );
            if (refChunk != null)
                Console.WriteLine("Is refArray: " + entry.IsRefArray);
            SceneNodeItemViewModel layerGraphItem = null;
            foreach (var layer in Layers)
            {
                if (!layer.HasDummyChild && layer.Children != null)
                    layerGraphItem = layer.Children.
                        Select(i => i as SceneNodeItemViewModel).
                        FirstOrDefault(j => j.SceneEntry.ID == entry.ID);
                if (layerGraphItem != null)
                    break;
            }
            
            if (layerGraphItem != null)
                layerGraphItem.IsSelected = true;

            if (DataChunks[0].IsExpanded && entry.ID < DataChunks[0].Children.Count)
                DataChunks[0].Children[entry.ID].IsSelected = true;

            if (selectedSceneEntry == null || selectedSceneEntry.SceneEntry.ID != entry.ID)
                SelectedSceneEntry = new SceneEntryPropertiesViewModel(entry);
        }

        void CreateLayerGraph()
        {
            Layers = sceneContainer.Layers.Select(i => new LayerItemViewModel(i, this)).ToList();
        }

        void CreateSceneGraph()
        {
            SceneGraphItems = new List<SceneNodeItemViewModel>() 
            { 
                new SceneNodeItemViewModel(sceneContainer.RootNode, this) 
            };
        }

        protected override void CreateDataChunks()
        {
            DataChunks = container.StreamChunks.Select(i => new ChunkItemViewModel(i, this)).ToList();
        }
    }
}
