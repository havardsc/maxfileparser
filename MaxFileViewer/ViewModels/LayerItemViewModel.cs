﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MaxFileViewer.Models;
using MaxFileViewer.ViewModels.Utilities;
using System.Collections.ObjectModel;

namespace MaxFileViewer.ViewModels
{
    class LayerItemViewModel : LazyTreeViewItem
    {
        SceneViewModel sceneViewModel;

        public LayerEntry LayerEntry { get; private set; }

        public override string DisplayName
        {
            get
            {
                return LayerEntry.ToString();
            }
            protected set
            {
                base.DisplayName = value;
            }
        }

        public LayerItemViewModel(LayerEntry layerEntry, SceneViewModel sceneViewModel)
            : base()
        {
            this.sceneViewModel = sceneViewModel;
            LayerEntry = layerEntry;
            isDummy = false;

            if (layerEntry.Children != null && layerEntry.Children.Count > 0)
            {
                children = new ObservableCollection<LazyTreeViewItem>();
                Children.Add(DummyChild);
            }
        }

        protected override void LoadChildren()
        {
            if (!isExpanded)
            {
                children.Clear();
                Children.Add(DummyChild);
                return;
            }

            children.Remove(DummyChild);

            foreach (var child in LayerEntry.Children)
                children.Add(new SceneNodeItemViewModel(child, sceneViewModel, false));
        }
    }
}
