﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MaxFileViewer.Models;

namespace MaxFileViewer.ViewModels
{
    class DocumentSummaryInformationViewModel : DefaultStreamViewModel
    {
        DocumentSummaryInformationContainer summaryContainer;

        public static new string DisplayName { get { return "Document Summary Information"; } }

        public List<DocumentSummaryInformationContainer.PropertyDataSet> Properties
        { get { return summaryContainer.Properties; } }

        public DocumentSummaryInformationViewModel(StreamContainer container)
            : base(container, DisplayName)
        {
            summaryContainer = container as DocumentSummaryInformationContainer;
        }
    }
}
